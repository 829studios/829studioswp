<?php
/**
 * The search form template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */
?>
<form method="get" action="<?php echo bloginfo('url'); ?>" >
 <input type="text" name="s" id="s" placeholder="Search the blog" />
 <input type="hidden" name="post_type" value="post">
 <input type="submit" value="search" />
</form>
