var $ = jQuery.noConflict();

var Project = {

	init: function()
	{
		$('html').removeClass('no-js');
		Project.initFormFields();
		Project.localScroll();
		Project.localScroll();
		Project.highlightActiveMenuItem();
		Project.accordion();
		Project.homeSlideshow();
		Project.setElemWidth('.blog-nav ul li');
		Project.setElemWidth('.top-menu ul li');
		Project.primaryWorkBoxHeight();
		Project.setBoxHeightEqualWidth('#secondary-works .single-sec-work');
		Project.showServiceList();
		Project.loadMorePosts();
		Project.tabs();
		Project.tabsHeadWidth();
		Project.tabsHeadSlide();
		Project.pageSliderInit();
		Project.disableButton();
		Project.customCheckbox();
		Project.bioLightbox();
		Project.setWindowHeight();
		Project.mobileMainNavigation();
		Project.mobileSubNavigation();
		Project.mobileCatNavigation();
		Project.mobileInnerNavArrows();
		Project.servicesOverlayHeight();
		Project.customScrollTable();
		Project.removeEmptyParagraph();
		Project.fullPager();
		Project.lightbox();
		Project.setElemWindowHeight('#team, #workspace');
		Project.iOSHover();
		//Project.pageLoaderOverlay();

		Project.homeServicesTiles();
		Project.mobileWorkTilesState();
	},

	/* =============================================================================
 		Main
	/* ========================================================================== */

	/***** FORM FIELDS VALUE *****/

	initFormFields : function()
	{
		$('input[type="text"], input[type="email"], input[type="tel"], input[type="password"], textarea').focus(function() {
			if ($(this).val() == $(this).prop('defaultValue')) {
				$(this).val('');
			}
		});

		$('input[type="text"], input[type="email"], input[type="tel"], input[type="password"], textarea').blur(function() {
			if ($(this).val() === '') {
				$(this).val($(this).prop('defaultValue'));
			}
		});
	},

	/***** TOUCH SCREEN DEVICES - ONE CLICK ANCHORS *****/

	iOSHover: function()
	{
		$('a').not('.lightbox-trigger, [data-lightbox], [data-tab], .service-link, .slick-next, .slick-prev, .close-bt').on('touchend', function(){
			var href = $(this).attr('href');
			window.location.href = href;
		});
	},

	/***** SMOOTH SCROLL *****/

	localScroll : function()
	{

		var mainHeaderHeight = $('#main-header').height(),
		submenuHeight = $('.inner-nav').height(),
		scrollOffset = mainHeaderHeight + submenuHeight;

		$('body').localScroll({
			offset: -scrollOffset
		});

	},

	/***** HIGHLIGHT ACTIVE MENU ITEM (ACTUAL SECTION) *****/

	highlightActiveMenuItem : function()
	{

		var item = $('.inner-nav ul li a');

		if (item.length > 0) {

			var pageOffsetTop = $('#main-header').outerHeight() + $('.inner-nav').outerHeight();
			var section = $('.main').find('section');
			var windowPosTop = $(window).scrollTop();

			section.each(function(){

				var sec = $(this);
				var secTop = sec.offset().top;
				var secHeight = sec.height();

				if (secTop < windowPosTop + pageOffsetTop && windowPosTop < (secTop + secHeight)) {
					var sectionId = sec.attr('id');
					var currentItem = $('.inner-nav ul li a[href="#'+sectionId+'"]');

					if (currentItem.length) {
						if (!currentItem.hasClass('active')) {
							currentItem.addClass('active');
						}
						item.not(currentItem).removeClass('active');
					}

				}

			});


		}

	},

	/********** ACCORDION **********/

	accordion : function()
	{
		$('.accordion .accordion-header').click(function() {
			if ($(this).next().is(':visible')) {
				$(this).parent().removeClass('open');
				$(this).next().filter(':not(:animated)').slideUp();
				$(this).removeClass('active');
			} else {
				$(this).parent().addClass('open');
				$(this).next().slideDown();
				$(this).addClass('active');
			}
		});
	},

	/***** HOME SLIDESHOW INIT *****/

	homeSlideshow : function()
	{

		var slider = $('.home-slideshow .slide-list');

		slider.slick({
			arrows: true,
		});

	},

	/***** SET ELEMENT WIDTH *****/

	setElemWidth : function(elem)
	{

		var item = $(elem);
		var itemLength = item.length;

		item.css('width', 100/itemLength + '%');

	},

	/***** SET ELEMENT WINDOW HEIGHT *****/

	setElemWindowHeight : function(elem)
	{

		var item = $(elem);
		var windowHeight = $(window).height();
		var headerHeight = $('#main-header').height();
		var topNav = $('nav.top-menu').height();

		item.css('height', windowHeight - headerHeight - topNav + 'px');

	},

	/***** SET HEIGTHT OF PRIMARY WORKS BOXES *****/

	primaryWorkBoxHeight : function()
	{

		var ratio = 0.75;
		var box = $('#primary-works .single-primary-work');
		var boxWidth = box.width();
		var boxHeight = boxWidth*ratio;

		box.css('height', boxHeight);

	},

	/***** SET BOX HEIGHT (SAME AS WIDTH) *****/

	setBoxHeightEqualWidth : function(box)
	{

		var box = $(box),
		boxWidth = box.width();

		box.css('height', boxWidth);

	},

	/***** SET HEIGHT OF HIGHEST BOX TO ALL *****/

	setMaxHeight : function(box)
	{

		$(box).css('height', 'auto');

			var boxHeight = $(box).map(function ()
			{
				return $(this).height();
			}).get(),

			maxHeight = Math.max.apply(null, boxHeight);

			$(box).height(maxHeight);

	},

	/***** SERVICE LIST DROPDOWN *****/

	showServiceList : function()
	{

		var trigger = $('.single-work-intro .det'),
		list = $('.single-work-intro .service-list-wrapper');

		trigger.on('click', function(e){
			e.preventDefault();
			if (!list.is(':animated')) {
				list.slideToggle();
				$(this).toggleClass('active');
			};
		});

	},

	/***** LOAD MORE POSTS ON BLOG PAGE *****/

	loadMorePosts : function()
	{

		var loadPostsButton = $('.more-posts.bt'),
		postsWrapper = $('section.posts');

		loadPostsButton.on('click', function(){
			$.ajax({
				url: SiteVars.ajaxUrl,
				async: true,
				type: 'POST',
				dataType: 'JSON',
				data: {
					action: 'get_ajax_posts',
					category: SiteVars.category,
					offset: SiteVars.offset
				},
				success: function(data)
				{
					postsWrapper.append(data);
					SiteVars.offset = +SiteVars.offset + +SiteVars.perPage;
					if (data.length < SiteVars.perPage) {
						loadPostsButton.hide();
					}
				},
			});
			return false;
		});

	},

	/***** TABS FUNCTIONALITY *****/

	tabs : function()
	{

		var tabsContainer = $('.tabs');

		if (tabsContainer.length > 0) {
			tabsContainer.each(function(){

				var tabHeads = $(this).find('.tab-head li');
				var tabBodies = $(this).find('.tab-body');
				var trigger = $(this).find('.tab-head a');
				var activeTabHeadId = $(this).find('.tab-head li.active a').data('tab');

				$(this).find('div#'+activeTabHeadId).addClass('active');
				
				function hashChangeTab( hash ) {
	
					if(hash == 'inquiry') {
						var currentTabId = 'tabs-2';
					} else if (hash == 'resume')  {
						var currentTabId = 'tabs-3';
					} else {
						return;
					}
				
					var currentTab = tabHeads.find('[data-tab="' + currentTabId + '"]');
					tabHeads.removeClass('active');
					currentTab.parent('li').addClass('active');
					tabBodies.removeClass('active').hide();
					currentTab.closest('.tabs').find('div#' + currentTabId).addClass('active').fadeIn();
					
					return;
				}
				
				if($('body').hasClass('contact')) {
					var param = document.URL.split('#')[1];
					if( param != null ) {
						hashChangeTab(param);
					}
				}
							
				trigger.on('click', function(e){
                    
					e.preventDefault();
					
					if (!$(this).parent('li').hasClass('active')) {
						tabHeads.removeClass('active');
						$(this).parent('li').addClass('active');
						tabBodies.removeClass('active').hide();
						var currentTabId = $(this).data('tab');
						$(this).closest('.tabs').find('div#'+currentTabId).addClass('active').fadeIn();
					}

				});
			});
		}
	},

	/***** SET TABS HEAD WIDTH *****/

	tabsHeadWidth : function()
	{

		var tabsContainer = $('.tabs');

		if (tabsContainer.length > 0) {
			tabsContainer.each(function(){
				var tabHead = $(this).find('ul.tab-head li');
				var tabHeadLenght =  tabHead.length;
				var tabHeadWidth = 100/tabHeadLenght;

				tabHead.css('width', tabHeadWidth+'%');
			});
		}

	},

	/***** PAGE GALLERY *****/

	pageSliderInit : function()
	{

		var gallery = $('.page-gallery');

		gallery.each(function(){
			$(this).slick({
				arrows: true,
				dots: true
			});
		});

	},

	/***** DISABLE BUTTONS *****/

	disableButton : function()
	{

		var button = $('.bt.disabled');

		button.on('click', function(e){
			e.preventDefault();
		});

	},

	/***** CUSTOM CHECKBOX & RADIO *****/

	customCheckbox : function()
	{

		var inputToStyle = $('.gform_body input[type="checkbox"], .gform_body input[type="radio"]');

		inputToStyle.ezMark();

	},

	/***** PERSON BIO LIGHBOX *****/

	bioLightbox : function()
	{

		var trigger = $('.person-box .text-box .bt'),
		overlay = $('#overlay'),
		personBox = $('.person-info'),
		personWrapper = $('.person-wrapper');

		if (personWrapper.length > 0) {
			var	personWrapperPosTop = personWrapper.offset().top;
		}

		trigger.on('click', function(e){
			e.preventDefault();

			var personBoxIndex = $(this).data('tab');

			personBox.each(function(){
				var boxIndex = $(this).data('tab');
				if (boxIndex == personBoxIndex) {
					$(this).fadeIn(300);
					$('html, body').animate({
						scrollTop: $(this).offset().top - 100
					}, 300);
				}

			});
			overlay.fadeIn(300);
		});

		var boxClose = $('.person-info .close-bt');

		boxClose.on('click', function(e){
			e.preventDefault();
			overlay.fadeOut(300);
			personBox.fadeOut(300);
			$('html, body').animate({
				scrollTop: personWrapperPosTop - 100
			}, 300);
		});

	},

	/***** SPEAKER BIO LIGHBOX *****/

	
	/***** ANIMATE PAGE TITLE *****/

	animateTitle : function()
	{

		var title = $('.animate-title');

		setTimeout(function(){
			title.addClass('visible');
		}, 300);

	},

	/***** SET ELEMENT WINDOW HEIGHT *****/

	setWindowHeight : function()
	{

		var windowH = $(window).height(),
		elem = $('.work-banner, .page-banner, #video-banner, .home-slideshow, #services-content .services-page-content .page-content, .services-box, #culture .slider-wrapper .page-gallery'),
		headerH = $('#main-header').height(),
		topNavH = $('nav.top-menu').height(),
		elemH = windowH - (headerH + topNavH);

		elem.not('.contact-banner').height(elemH);

	},

	servicesOverlayHeight : function()
	{

		var wrapper = $('.services-box .single-service-box .single-service-content'),
		wrapperH = wrapper.height(),
		box = $('.services-box .single-service-box .single-service-overlay');

		box.height(wrapperH);

	},


	/* =============================================================================
 			Mobile
	/* ========================================================================== */


	/***** TABS HEAD SLIDE (MOBILE) *****/

	tabsHeadSlide : function()
	{

		var tabsContainer = $('.tabs');

		if (tabsContainer.length > 0) {
			tabsContainer.each(function(){
				var tabHeadSlider = $(this).find('div.tab-head.mobile-tab-head');

				tabHeadSlider.slick({
					arrows: true,
					prevArrow: '<a href="#" class="slick-prev">Prev</a>',
					nextArrow: '<a href="#" class="slick-next">Next</a>'
				});

				tabHeadSlider.on('afterChange', function(){
					var activeSlide = $(this).find('.slick-active a');
					activeSlide.trigger('click');
				});

			});
		}

	},

	/***** MAIN NAVIGATION (MOBILE) *****/

	mobileMainNavigation : function()
	{

		var trigger = $('.nav-trigger'),
		menu = $('#main-navigation'),
		menuItem = menu.find('li'),
		menuItemLength = menuItem.length;

		if (menuItemLength%2===0) {
			var menuItemPartOfMenu = menuItemLength/2;
		}
		else {
			var menuItemPartOfMenu = (menuItemLength+1)/2;
		}

		trigger.on('click', function(e){
			e.preventDefault();


			var headerHeight = $('#main-header').height();

			var menuHeight = window.innerHeight - headerHeight;

			$(this).toggleClass('open');

			if (!menu.is(':animated')) {

				menu.height(menuHeight);
				menuItem.height(menuHeight/menuItemPartOfMenu);
				menu.slideToggle().toggleClass('visible');
				menu.css('top', headerHeight);

				if ($(this).hasClass('open')) {

					var i = 0;
					setInterval(function(){
					if (menuItem.length > i) {
							menuItem.eq(i).addClass('visible');
							i++;
						}
					}, 200);

				}
				else {
					menuItem.removeClass('visible');
				}

			}

			$(window).on('resize', function(){
				headerHeight = $('#main-header').height();
				menuHeight = window.innerHeight - headerHeight;

				if (!menu.is(':animated') && menu.hasClass('visible')) {
					menu.height(menuHeight);
					menuItem.height(menuHeight/menuItemPartOfMenu);
					menu.css('top', headerHeight);
				}

			});


		});

	},

	/***** SECOND NAVIGATION (MOBILE) *****/

	mobileSubNavigation : function()
	{

		var trigger = $('.top-menu-trigger'),
		menu = $('nav.mobile-top-menu');

		trigger.on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('open');
			if (!menu.is(':animated')) {
				menu.slideToggle();
			}
		});

	},

	/***** CATEGORY NAVIGATION (MOBILE) *****/

	mobileCatNavigation : function()
	{

		var trigger = $('.cat-trigger'),
		menu = trigger.siblings('ul');

		trigger.on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('open');
			if (!menu.is(':animated')) {
				menu.slideToggle();
			}
		});

	},

	/***** INNER NAVIGATION ARROWS (MOBILE) *****/

	mobileInnerNavArrows : function()
	{

		var trigger = $('.top-menu-item');

		trigger.on('click', function(e){
			e.preventDefault();

			var activeMenuItem = $('nav.top-menu.inner-nav ul li a.active'),
			prevItem = activeMenuItem.parent().prev().find('a'),
			nextItem = activeMenuItem.parent().next().find('a');

			if ($(this).hasClass('section-nav-prev')) {
				prevItem.trigger('click');
			}
			else {
				nextItem.trigger('click');
			}
		});

	},

	/***** CUSTOM SCROLL *****/

	customScrollTable : function()
	{

		if ($(window).width() < 767) {

			$('.table-responsive').addClass('mCustomScrollbar');
			$('.table-responsive').mCustomScrollbar({
	    axis:"x"
				});
		}
		else {
			$('.table-responsive').removeClass('mCustomScrollbar');
			$('.table-responsive').mCustomScrollbar('destroy');
		}

	},
	removeEmptyParagraph : function()
	{

		$('p:empty').remove();

	},

	fullPager : function()
	{
		var sections = $('.case-study-content .section').length;


    	$('.case-study-content').fullpage({
	    	scrollingSpeed: 700,
	    	navigation: false,
	    	touchSensitivity: 15,
	    	autoScrolling: true
	    });


	},

	lightbox : function()
	{
		$('.lightbox-trigger').click(function(e){
			e.preventDefault();
			var lightboxID = $(this).data('lightbox');
			var activeLightbox = $('#lightbox').attr('data-lightbox');
			$('html').addClass('lightbox-opened');
			$('#lightbox').fadeIn(200);
			setTimeout(function(){
				lightboxAjax(lightboxID);
			}, 300);
		});



		function lightboxAjax(id)
		{
			jQuery.ajax({
	            url: SiteVars.ajaxUrl,
	            async: false,
	            type: 'POST',
	            dataType: 'html',
	            data: {
	                action: 'lightbox_ajax',
	                lightboxid: id,
	                lang:   SiteVars.lang
	            },
	            success: function(data) {
	            	$('#lightbox').attr('data-lightbox', id);
	            	$('#lightbox .wrapper').append(data);
	            	$('#lightbox .wrapper').addClass('active');
	            	$('.lightbox-slider').on('afterChange',function(e,o){
					    //on change slide = do action
					    $('iframe').each(function(){
					        $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
					    });
					}).slick();
	            	$('#lightbox .loader').fadeOut(200);
	            	lightboxClose();
	            }
	        });
		}

		function lightboxClose()
		{
			$('#lightbox .close').click(function(e){
				e.preventDefault();
				$('#lightbox').fadeOut(200);
				$('html').removeClass('lightbox-opened');
				$('#lightbox .wrapper').empty().removeClass('active');
			});
		}
	},
	//initialize page overlay
	pageLoaderOverlay: function()
	{
		$('.page-overlay-loader .loader-ico-wrap.fill-wrap').addClass('active');
		setTimeout(function() {
			$('.page-overlay-loader').fadeOut();
		}, 2000);
	},
	//Page Loader Complete
	pageLoaderOverlayComplete: function()
	{
		var pageLoader = $('.page-overlay-loader');
		
		if (pageLoader.hasClass("loaded")) {
				hideLoader();
		} else {
			$(document).on("pageLoaderEvent", function(){
				hideLoader();
			});
		}
		
		function hideLoader() {
			pageLoader.fadeOut();
			clearInterval(loaderInterval);
			sessionStorage.firstLoad = true;
		}
	},
	// Home & Services tiles mobile
	homeServicesTiles: function()
	{
		$('.service-link').on('click', function(e) {
			var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
			if(isiDevice) {
				if(!$(this).hasClass('service-hover-active')) {
					e.preventDefault();
					$('.service-hover-active').removeClass('service-hover-active');
					$(this).addClass('service-hover-active');
				}
			}
		});
	},
	// Add active states to work tiles on mobile
	mobileWorkTilesState: function()
	{
		$('.single-primary-work').on('click', function() {
			var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
			if(isiDevice) {
				if(!$(this).hasClass('primary-work-active')) {
					$('.primary-work-active').removeClass('primary-work-active');
					$(this).addClass('primary-work-active');
					$(this).find('.box-content').addClass('primary-work-active');
				}
			}
		});
	}
};

jQuery(document).ready(
	function()
	{
		Project.init();

		$(window).scroll(function(){
			Project.highlightActiveMenuItem();
		});

		$(window).resize(function(){
			Project.primaryWorkBoxHeight();
			Project.setWindowHeight();
			Project.servicesOverlayHeight();
			Project.customScrollTable();
			Project.setMaxHeight('.box-info-wrapper .single-box-info');
			Project.setBoxHeightEqualWidth('#secondary-works .single-sec-work');
			Project.setElemWindowHeight('#team, #workspace');
		});
	});

$(window).load(function(){
	Project.animateTitle();
	Project.setMaxHeight('.box-info-wrapper .single-box-info');
	Project.pageLoaderOverlayComplete();
});


