<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $workID = get_the_id(); ?>

<!-- =========== SINGLE WORK ========= -->

<seciton class="single-work-section">
	<?php $thumbId = get_post_thumbnail_id();
	$thumbSrc = wp_get_attachment_image_src($thumbId, 'full');
	$workLogo = get_field('single_work_logo');
	$introText = get_field('work_intro_text'); ?>
	<?php if ($thumbId) : ?>
		<div class="work-banner" style="background-image: url('<?php echo $thumbSrc[0]; ?>');">
			<?php if ($workLogo) : ?>
				<span class="logo-wrapper">
					<img src="<?php echo $workLogo['url']; ?>" alt="<?php echo $workLogo['alt']; ?>">
				</span>
			<?php endif; ?>
		</div><!-- /.work-banner -->
	<?php endif; ?>
</seciton><!-- /.single-work-section -->

<section class="single-work-intro">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<?php the_field('work_intro_text'); ?>

		<?php $parentServices = wp_get_post_terms($workID, 'service'); ?>

		<?php if (!empty($parentServices)) : ?>
			<h3><?php _e('Services', '829Studios'); ?></h3>
			<ul class="parent-services">
				<?php $c = 1; ?>
				<?php foreach($parentServices as $topListService) : ?>
					<?php if ($topListService->parent == 0) : ?>
						<li><?php if($c != 1) : ?><span>&sol;</span> <?php endif; ?><a href="<?php echo get_term_link($topListService->slug, 'service'); ?>"><?php echo $topListService->name; ?></a></li>
						<?php $c++; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>		

		<?php $services = wp_get_post_terms($workID,'service'); ?>

		<?php if (!empty($services)) : ?>
			<div class="service-list-wrapper">
			<?php foreach ($services as $topService) : ?>
					<?php if ($topService->parent == 0) : ?>
						<ul class="service-list">
							<?php $id = $topService->term_id; ?>
							<?php $subServices = wp_get_post_terms($workID, 'service'); ?>
							<li><?php echo $topService->name; ?></li>
							<?php foreach($subServices as $subService) : ?>
								<?php if ($id == $subService->parent) : ?>
									<li><a href="<?php echo get_term_link($subService->slug, 'service'); ?>"><?php echo $subService->name; ?></a></li>
								<?php endif ?>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<a href="#" class="det"><span class="text1">see</span><span class="text2">hide</span> details</a>

	</div><!-- .container -->
</section><!-- /.single-work-intro -->

<section class="single-work-content single-page-content">
	<div class="container">
		<?php the_content(); ?>
	</div><!-- /.container -->
	<?php if (have_rows('add_page_section')) : ?>
					<?php while(have_rows('add_page_section')) : the_row(); ?>

					<?php if (get_row_layout() == 'deafult_content') : ?>

						<!-- Default Content -->

							<?php $content = get_sub_field('section_default_content');
							$removeTopMargin = get_sub_field('remove_top_margin');
							$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
							<div class="container<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
								<div class="row">
									<div class="col-lg-12">
										<?php echo $content; ?>
									</div><!-- /.col-lg-12 -->
								</div><!-- /.row -->
							</div><!-- /.container -->

						<?php elseif (get_row_layout() == 'custom_background_color_section') : ?>

							<!-- Custom Background Color Section -->

							<?php $bgColor = get_sub_field('section_custom_bg_color_color');
							$content = get_sub_field('section_custom_bg_color_content');
							$removeTopMargin = get_sub_field('remove_top_margin');
							$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
							<section class="custom-bg-color<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>" style="background-color: <?php echo $bgColor; ?>;">
								<div class="container">
									<div class="row">
										<div class="col-lg-12">
											<?php echo $content; ?>
										</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->
								</div><!-- /.container -->
							</section><!-- /.custom-bg-color -->

							<?php elseif (get_row_layout() == 'custom_background_pattern_section') : ?>

								<!-- Custom Background Pattern Section -->

								<?php $bgPatternId = get_sub_field('section_custom_bg_pattern_pattern');
								$bgPatternSrc = wp_get_attachment_image_src($bgPatternId, 'full');
								$content = get_sub_field('section_custom_bg_pattern_content');
								$removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
								<section class="custom-bg-pattern<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>" style="background: url('<?php echo $bgPatternSrc[0]; ?>');">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<?php echo $content; ?>
											</div><!-- .col-lg-12 -->
										</div><!-- /.row -->
									</div><!-- /.container -->
								</section><!-- .custom-bg-pattern -->

							<?php elseif (get_row_layout() == 'page_full_width_image') : ?>

								<!-- Full Width Image Section -->

								<?php $img = get_sub_field('add_page_full_width_image');
								$removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>

								<img class="page-full-width-img<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">

								<?php elseif (get_row_layout() == 'page_full_width_gallery') : ?>

								<!-- Full Width Gallery Section -->

								<?php $slideshow = get_sub_field('full_width_gallery_shortcode');
								$removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
								<section class="full-width-gallery<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
									<?php echo do_shortcode($slideshow); ?>
								</section><!-- .full-width-gallery -->

							<?php elseif (get_row_layout() == '50_50_images') : ?>

								<!-- 50-50 Images -->

								<section class="half-images clearfix<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
									<?php $leftImg = get_sub_field('50_50_images_left_image');
									$rightImg = get_sub_field('50_50_images_right_image');
									$removeTopMargin = get_sub_field('remove_top_margin');
									$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
									<img src="<?php echo $leftImg['url']; ?>" alt="<?php echo $leftImg['alt']; ?>" class="img-left">
									<img src="<?php echo $rightImg['url']; ?>" alt="<?php echo $rightImg['alt']; ?>" class="img-right">
								</section><!-- .half-images -->

								<?php elseif(get_row_layout('full_width_content')) : ?>

								<!-- Full Width Content -->
								<?php $removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>

								<section class="full-width-content clearfix<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
									<?php $content = get_sub_field('section_full_width_content'); ?>
									<?php if ($content) : ?>
										<?php echo remove_empty_paragraphs($content); ?>
									<?php endif; ?>


								</section><!-- .full-width-content -->

								<?php endif; ?>

					<?php endwhile; ?>
				<?php endif; ?>
</section><!-- /.single-work-content -->

<?php $worksPage = get_field('select_works_page', 'option'); ?>

<nav class="works-nav">
	<div class="container">
		<ul>
			<li class="prev"><?php previous_post_link('%link', '<span class="hidden-xs">Previous Project</span><span class="visible-xs">Previous</span>'); ?></li>
			<?php if ($worksPage) : ?>
			<li class="all"><a href="<?php echo get_permalink($worksPage); ?>"><span class="hidden-xs"><?php _e('All Projects', '829Studios'); ?></span><span class="visible-xs"><?php _e('All', '829Studios'); ?></span></a></li>
			<?php endif; ?>
			<li class="next"><?php next_post_link('%link', '<span class="hidden-xs">Next Project</span><span class="visible-xs">Next</span>'); ?></li>

		</ul>
	</div><!-- /.container -->
</nav><!-- /.works-nav -->

<?php if (have_rows('add_similar_project')) : ?>
	<section class="similar-projects">
		<div class="container">
			<h2><?php _e('Similar Projects', '829Studios'); ?></h2>
			<div class="row">
				<?php while(have_rows('add_similar_project')) : the_row(); ?>
					<div class="similar-project-box col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php $id = get_sub_field('select_similar_project');
						$thumbId = get_field('similar_project_image', $id);
						$thumbSrc = wp_get_attachment_image_src($thumbId, 'similar-work-thumb');
						$title =  get_the_title($id);
						$url = get_permalink($id); ?>
						<?php if ($thumbSrc) : ?>
							<a href="<?php echo $url; ?>"><img src="<?php echo $thumbSrc[0]; ?>" alt="project-image"></a>
						<?php endif; ?>
						<?php if ($title) : ?>
							<h3><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h3>
						<?php endif; ?>
					</div><!-- .similar-project-box -->
				<?php endwhile; ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</section><!-- /.similar-projects -->
<?php endif; ?>

<?php get_footer(); ?>
