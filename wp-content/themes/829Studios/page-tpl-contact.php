<?php
/**
 * Template Name: Contact Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $bgVerticalPos = get_field('set_vertical_pos');
$bgHorizontalPos = get_field('set_horizontal_pos'); ?>

<!-- =========== CONTACT BANNER ========= -->

<?php $thumbId = get_post_thumbnail_id();
$thumbSrc = wp_get_attachment_image_src($thumbId, 'work-thumb'); ?>

	<section class="page-banner contact-banner"<?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($thumbId) : ?>background-image:url('<?php echo $thumbSrc[0]; ?>');<?php endif; ?>"<?php endif; ?>>
		<div class="page-banner-content">
			<h1 class="page-title animate-title"><?php the_title(); ?></h1>
		</div><!-- /.page-banner-content -->
	</section><!-- /.page-banner -->

<!-- =========== CONTACT CONTENT ========= -->

<section id="contact-content" class="single-page-content">
	<?php $address = get_field('contact_address');
	$phone = get_field('contact_phone');
	$email = get_field('contact_email'); ?>
	<?php if ($address || $phone || $email) : ?>
		<div class="contact-info">
			<div class="container">
				<div class="row">
					<?php if ($phone) : ?>
						<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 phone">
							<div class="box-content-wrapper">
								<p><span></span><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
							</div><!-- /.box-content-wrapper -->
						</div><!-- /.phone -->
					<?php endif; ?>
					<?php if ($address) : ?>
						<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 address">
							<div class="box-content-wrapper">
								<?php echo $address; ?>
							</div><!-- /.box-content-wrapper -->
						</div><!-- /.address -->
					<?php endif; ?>
					<?php if ($email) : ?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 email">
							<div class="box-content-wrapper">
								<p><span></span><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
							</div><!-- /.box-content-wrapper -->
						</div><!-- /.email -->
					<?php endif; ?>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.contact-info -->
	<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php the_content(); ?>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /#contact-content -->

<?php get_footer(); ?>
