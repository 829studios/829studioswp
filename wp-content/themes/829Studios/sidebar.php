<?php
/**
 * Primary widget area
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */
?>
<aside class="widget-area">
 <?php if(is_active_sidebar('primary_widget_area')) : ?>
  <?php dynamic_sidebar('primary_widget_area'); ?>
 <?php endif; ?>
</aside>