<?php
/**
 * Template Name: Works Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<!-- =========== WORKS PAGE ========= -->

<section id="works-content">
	<div class="container">
		<?php the_content(); ?>
	</div><!-- /.container -->
</section><!-- /#works-content -->

<!-- =========== PRIMARY WORKS ========= -->

<?php if (have_rows('add_primary_work')) : ?>
	<section id="primary-works">
		<?php while(have_rows('add_primary_work')) : the_row(); ?>
		<?php $workBgId = get_sub_field('primary_work_background');
		$workBgSrc = wp_get_attachment_image_src($workBgId, 'full');
		$workLogo = get_sub_field('primary_work_logo');
		$workTitle = get_sub_field('primary_work_title');
		$workDesc = get_sub_field('primary_work_description');
		$workUrl = get_sub_field('primary_work_read_more_url');
		$hoverColor = get_sub_field('primary_work_hover_color');  ?>
			<div class="single-primary-work"<?php if($workBgSrc) : ?> style="background-image: url('<?php echo $workBgSrc[0]; ?>');"<?php endif; ?>>
				<?php if ($workLogo) : ?>
					<span class="logo-wrapper">
						<img src="<?php echo $workLogo['url']; ?>" alt="<?php echo $workLogo['alt']; ?>">
					</span>
				<?php endif; ?>
				<div class="box-content">
					<?php if ($hoverColor) : ?>
						<span class="box-bg" style="background-color: <?php echo $hoverColor; ?>;"></span>
					<?php endif; ?>
					<div class="box-content-text-link-wrap">
						<div class="box-text-wrapper">
							<?php if ($workTitle) { echo "<h4>".$workTitle."</h4>"; } ?>
							<?php if ($workDesc) { echo $workDesc; } ?>
						</div><!-- /.box-text-wrapper -->
						<div class="box-text-link">
							<?php if ($workUrl) { echo "<a class='bt' href=".$workUrl.">".__("read more", "829Studios")."</a>"; } ?>
						</div><!-- /.box-text-link -->
					</div><!-- box-content-text-link-wrap -->
				</div><!-- /.box-content -->
			</div><!-- /.single-primary-work -->
		<?php endwhile; ?>
	</section><!-- /#primary-works -->
<?php endif; ?>

<!-- =========== SECONDARY WORKS ========= -->

<?php if (have_rows('add_sec_work')) : ?>
	<section id="secondary-works">
		<?php $topText = get_field('sec_works_section_text'); ?>
		<?php if ($topText) : ?>
			<div class="section-text">
				<div class="container">
					<?php echo $topText; ?>
				</div><!-- /.container -->
			</div><!-- /.section-text -->
		<?php endif; ?>
		<div class="container">
			<div class="row">
			<?php while(have_rows('add_sec_work')) : the_row(); ?>
			<?php $workBgId = get_sub_field('sec_work_background');
			$workBgSrc = wp_get_attachment_image_src($workBgId, 'full');
			$workLogo = get_sub_field('sec_work_logo');
			$workTitle = get_sub_field('sec_work_title');
			$workDesc = get_sub_field('sec_work_description');
			$workUrl = get_sub_field('sec_work_link');
			$hoverColor = get_sub_field('sec_work_hover_color');  ?>
					<div class="single-sec-work"<?php if($workBgSrc) : ?> style="background-image: url('<?php echo $workBgSrc[0]; ?>');"<?php endif; ?>>
						<?php if ($workUrl) : ?>
							<a href="<?php echo $workUrl; ?>" target="_blank" class="sec-work-link"></a>
						<?php endif; ?>
					<?php if ($workLogo) : ?>
						<span class="logo-wrapper">
							<img src="<?php echo $workLogo['url']; ?>" alt="<?php echo $workLogo['alt']; ?>">
						</span>
					<?php endif; ?>
					<div class="box-content">
						<?php if ($hoverColor) : ?>
							<span class="box-bg" style="background-color: <?php echo $hoverColor; ?>;"></span>
						<?php endif; ?>
						<div class="box-text-wrapper">
							<?php if ($workTitle) { echo "<h4>".$workTitle."</h4>"; } ?>
							<?php if ($workDesc) { echo $workDesc; } ?>
						</div><!-- /.box-text-wrapper -->
					</div><!-- /.box-content -->
				</div><!-- /.single-sec-work -->
			<?php endwhile; ?>
			</div><!-- /.row -->
			<div class="secondary-works-bottom-content-text">
				<?php the_field('bottom_section_text'); ?>
			</div><!-- secondary-works-content-text -->
		</div><!-- /.container -->
	</section><!-- /#secondary-works -->
<?php endif; ?>


<?php get_footer(); ?>
