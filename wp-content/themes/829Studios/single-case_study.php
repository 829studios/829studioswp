<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<!-- =========== SINGLE CASE STUDY ========= -->

	<?php $bgCol = get_field('case_study_background_color');
		if($is_iphone && get_field('case_study_mobile_background_image')) {
			$bgSrc = get_field('case_study_mobile_background_image');
		} else {
			$bgSrc = get_field('case_study_background_image');
		}
		$bgVerticalPos = strtolower(get_field('case_study_background_image_vertical_alignment'));
		$bgHorizontalPos = strtolower(get_field('case_study_background_image_horizontal_alignment'));
		$bgType = get_field('case_study_background_image_type');
		$client = get_field('case_study_client_name'); ?>


	<main class="case-study-content">

		<section class="section full-section case-study-cover <?php echo $bgType; ?>" style="background-color: <?php echo $bgCol; ?>; background-image: url(<?php echo $bgSrc; ?>); background-position: <?php echo $bgVerticalPos . ' ' . $bgHorizontalPos; ?>;">
			<div class="section-content">
				<header>
					<?php if ($client): ?>
						<?php if(get_field('client_text_color')): ?>
							<style>
								.case-study-cover header h6:after {
									background: <?php the_field('client_text_color'); ?>;
								}
							</style>
						<?php endif; ?>
						<h6 <?php if(get_field('client_text_color')): ?> style="color:<?php the_field('client_text_color'); ?>;"<?php endif; ?>><?php echo $client; ?></h6>
						<h1 <?php if(get_field('title_text_color')): ?> style="color:<?php the_field('title_text_color'); ?>;"<?php endif; ?>><?php the_title(); ?></h1>
					<?php endif; ?>
				</header>
			</div>

		</section>

		<?php require_once "case_study-sections.php"; ?>
	</main>


<?php get_footer(); ?>
