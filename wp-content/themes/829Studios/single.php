<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<!-- =========== SINGLE POST ========= -->

<?php $blogBgId = get_field('blog_background', 'option');
$blogBgSrc = wp_get_attachment_image_src($blogBgId, 'blog-bg'); ?>

<section class="single-post-section">
	<div class="post-banner"<?php if($blogBgId) : ?> style="background-image: url('<?php echo $blogBgSrc[0]; ?>');"<?php endif; ?>>
		<div class="blog-banner-content">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<?php $authorId = get_the_author_meta( 'ID'); ?>
			<?php $authorPhoto = get_field('user_photo','user_'.$authorId); ?>
			<div class="post-meta">
				<span class="author"><?php the_author_posts_link(); ?></span>
				<?php if ($authorPhoto) : ?>
				<?php echo wp_get_attachment_image( $authorPhoto ,'author-thumb', false, array('class' => 'author-photo')); ?>
				<?php endif; ?>
				<span class="date"><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m'));?>"><?php echo get_the_date(get_option('date_format')); ?></a></span>
			</div><!-- /.post-meta -->
		</div><!-- /.blog-banner-content -->
</div><!-- /.post-banner -->
<div class="container-fluid">
	<div class="row">
		<article class="single-post col-lg-10">
			<?php the_content(); ?>
			<span class="share">
				<span class='st_facebook_large' st_url="<?php the_permalink(); ?>"></span>
				<span class='st_twitter_large' st_url="<?php the_permalink(); ?>"></span>
				<span class='st_linkedin_large' st_url="<?php the_permalink(); ?>"></span>
			</span>
		</article><!-- /.single-post -->
	</div><!-- /.row -->
</div><!-- .container -->
</section><!-- /.single-post-section -->

<?php if (have_rows('add_related_posts')) : ?>
	<section class="related-posts">
		<div class="container">
			<div class="row">
					<h2><?php _e('Related Posts', '829Studios'); ?></h2>
				<?php while(have_rows('add_related_posts')) : the_row(); ?>
					<div class="rel-post col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php $relPostId = get_sub_field('single_related_post');
						$relPostThumb = get_the_post_thumbnail($relPostId, 'rel-post');
						$relPostTitle = get_the_title($relPostId);
						$relPostUrl = get_permalink($relPostId); ?>
						<?php if ($relPostThumb) : ?>
							<a href="<?php echo $relPostUrl; ?>"><?php echo $relPostThumb; ?></a>
						<?php endif; ?>
						<h3><a href="<?php echo $relPostUrl; ?>"><?php echo $relPostTitle; ?></a></h3>
					</div><!-- /.rel-post -->
				<?php endwhile; ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.related-posts -->
<?php endif; ?>

<?php get_footer(); ?>
