<?php
/**
 * Template Name: Services Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $bgVerticalPos = get_field('set_vertical_pos');
$bgHorizontalPos = get_field('set_horizontal_pos'); ?>

<!-- =========== SERVICES BANNER ========= -->

<?php $thumbId = get_post_thumbnail_id();
$thumbSrc = wp_get_attachment_image_src($thumbId, 'full'); ?>

	<section id="services-banner" class="page-banner services-banner"<?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($thumbId) : ?>background-image:url('<?php echo $thumbSrc[0]; ?>');<?php endif; ?>"<?php endif; ?>>
		<div class="page-banner-content">
			<h1 class="page-title animate-title"><?php the_title(); ?></h1>
		</div><!-- /.page-banner-content -->
	</section><!-- /.page-banner -->

<!-- =========== SERVICES CONTENT ========= -->

<section id="services-content">
	<div class="services-page-content clearfix">
		<div class="page-content">
			<div class="content-wrapper">
				<?php the_content(); ?>
			</div><!-- /.content-wrapper -->
		</div><!-- /.page-content -->

		<?php $servicesParentArgs = array('parent' => 0, 'hide_empty' => false);
		$parentServices = get_terms('service', $servicesParentArgs); ?>

		<div class="services-box">
			<?php if (!empty($parentServices)) : ?>

				<?php foreach($parentServices as $service) : ?>

					<div class="single-service-box col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<div class="single-service-wrapper">
							<div class="single-service-content">
								<?php $serviceId = $service->term_id; ?>
								<?php $serviceIcon = get_field('service_main_icon', 'service_'.$serviceId);
								$serviceIconIE = get_field('service_main_icon_ie', 'service_'.$serviceId);
								$name = $service->name; ?>
								<?php if ($serviceIcon) : ?>
									<object  data="<?php echo $serviceIcon['url'] ?>" type="image/svg+xml"><img src="<?php echo $serviceIcon['url'] ?>"></object>
								<?php endif; ?>
								<!--[if lt IE 9]>
								<?php if($serviceIconIE) : ?>
									<img src="<?php echo $serviceIconIE['url']; ?>" alt="<?php echo $serviceIconIE['alt']; ?>">
								<?php endif; ?>
								<!<![endif]-->
								<h6><?php echo $name; ?></h6>
								<div class="single-service-overlay">
									<a href="<?php echo get_term_link($service->slug, 'service'); ?>" class="service-link"></a>
									<div class="overlay-content">
										<h6><?php echo $name; ?></h6>
										<?php echo apply_filters("the_content", stripslashes($service->description)); ?>
									</div><!-- /.overlay-content -->
								</div><!-- /.single-service-overlay -->
							</div><!-- /.single-service-content -->
						</div>
					</div><!-- /.single-service-box -->

				<?php endforeach; ?>

			<?php endif; ?>
		</div><!-- /.services-box -->
	</div><!-- /.services-page-content -->

	<?php $connectBg = get_field('connect_background');
	$connectTitle = get_field('connect_title');
	$connectText = get_field('connect_text');
	$connectLink = get_field('connect_link');

	if($connectBg) : ?>

	<section id="connect" style="background-image: url('<?php echo $connectBg['url']; ?>');">
		<div class="info-circle">
			<div class="circle-text">
				<?php if ($connectLink) : ?>
					<a href="<?php echo $connectLink; ?>" target="_blank" class="connect-link"></a>
				<?php endif; ?>
				<?php if ($connectTitle) : ?>
					<h6><?php echo $connectTitle; ?></h6>
				<?php endif; ?>
				<?php if ($connectText) : ?>
					<p><?php echo $connectText; ?></p>
				<?php endif; ?>
			</div><!-- ./circle-text -->
		</div><!-- /.info-circle -->
	</section><!-- .#connect -->

<?php endif; ?>
</section><!-- /#services-content -->


<?php get_footer(); ?>
