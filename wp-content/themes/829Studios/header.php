<?php

/**

* The Header for theme.

*

* Displays all of the <head> section and page header

*

* @package WordPress

* @subpackage 829Studios

* @since 829Studios 1.0

*/

?>

<!DOCTYPE html>

<!--[if IE 8]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lt9"><![endif]-->

<!--[if IE 9]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9 "><![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>

<script>
  window.intercomSettings = {
    app_id: "mbz07xzf"
  };

</script>
<!-- Start of HubSpot Embed Code --> 
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3391446.js"></script> 
<!-- End of HubSpot Embed Code -->
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>



	<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/ec4bee57-264b-4ec7-ba79-13e0f9204fd6.css"/>

	<?php wp_head(); ?>

	<script type="text/javascript">var switchTo5x=true;</script>



<link type="text/css" rel="stylesheet" href="/wp-content/themes/829Studios/css/vendor/chosen.min.css"/>




 <!--[if lt IE 9]>

   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

   <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

   <![endif]-->



  <!--[if lt IE 9]>

    <style type="text/css">

      .gform_body .gform_fields .ez-radio.ez-selected:before, .gform_body .gform_fields .ez-radio, .post-meta .author-photo, .info-circle {

        behavior: url('<?php bloginfo('template_directory'); ?>/PIE.php');

      }

    </style>

    <style type="text/css">

      .contact-info .email span, .contact-info .phone span, #video-banner, .slick-slide, #bottom-boxes .bottom-box, .post-banner, .blog-banner, .page-banner, .author-photo, .single-primary-work, .single-sec-work, .work-banner, .quote, .person-box, .slick-track > div, #connect, .footer-logo, .social-list li {

        -ms-behavior: url('<?php bloginfo('template_directory'); ?>/backgroundsize.min.htc');

      }

    </style>

    <!<![endif]-->

<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3391446.js"></script>
<!-- End of HubSpot Embed Code -->

   </head>

   <body class="<?php echo custom_body_class(); ?>">



<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NVCJWW"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-NVCJWW');</script>

<!-- End Google Tag Manager -->

   	<?php if (is_page_template('page-tpl-about.php')) : ?>

   		<div id="overlay"></div>

   	<?php endif ?>

   		<div id="lightbox" data-lightbox="">
	   		<div class="wrapper">

	   		</div>
				<div class="loader-wrapper">
					<div class="loader"></div>
				</div>
   		</div>

   	<section id="page" style="visibility:hidden;">



   		<!-- =========== HEADER ========= -->



   		<header id="main-header">

   			<a href="<?php bloginfo('url'); ?>" class="main-logo"></a>

   			<a href="#" class="nav-trigger visible-xs">
        <img src="<?php echo get_bloginfo('template_directory'); ?>/images/project-profile-menu.svg" alt="829 Studios" onerror="this.src='<?php echo get_bloginfo('template_directory'); ?>/images/project-profile-menu.png'">
        <img src="<?php echo get_bloginfo('template_directory'); ?>/images/project-profile-close.svg" alt="829 Studios" onerror="this.src='<?php echo get_bloginfo('template_directory'); ?>/images/project-profile-close.png'">
        </a>

	  				<nav id="main-navigation">

	  					<?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false, 'items_wrap' => '<ul class="%2$s">%3$s</ul>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>

	  				</nav><!-- /#main-navigation -->
<!-- reCAPTCHA script -->

<script src='https://www.google.com/recaptcha/api.js'></script>



   		</header><!-- /#main-header -->

   		<section class="main">
