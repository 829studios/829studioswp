<?php
/**
 * Template Name: Home Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $bgVerticalPos = get_field('set_vertical_pos');
$bgHorizontalPos = get_field('set_horizontal_pos'); ?>

<!-- =========== BANNER ========= -->


<?php $bannerImgSrc = get_field('home_banner_bg');
$bannerText = get_field('home_banner_text'); ?>




	<section id="video-banner" <?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($bannerImgSrc) : ?>background-image:url('<?php echo $bannerImgSrc; ?>');<?php endif; ?>"<?php endif; ?>>
		

<?php
require_once ('lib/Mobile_Detect.php');
$detect = new Mobile_Detect;
 if (!$detect->isMobile()) {  
 

?>


<?php $videoMp4 = get_field('video_mp4_src');
		$videoOgg = get_field('video_ogg_src'); ?>
	



  
	<?php if ($videoMp4 || $videoOgg) : ?>
			<video autoplay loop muted >
				<source src="https://www.829llc.com/wp-content/themes/829Studios/media/611715235.webm" type="video/webm"
				<source src="<?php echo $videoMp4; ?>" type="video/mp4">
				<source src="<?php echo $videoOgg; ?>" type="video/ogg">
			</video>
			<?php endif; ?>

<?php }  ?>
	

		<?php if ($bannerText) : ?>
			<div class="container">
				<div class="banner-text-wrapper">
					<?php echo $bannerText; ?>
				</div><!-- /.banner-text-wrapper -->
			</div><!-- /.container -->
		<?php endif; ?>
	</section><!-- /#video-banner -->

	<?php $logoStripeID = get_field('home_logo_stripe');
	$logoStripeSrc = wp_get_attachment_image_src($logoStripeID, 'logo-stripe');
	$logoStripeRetina = wr2x_get_retina_from_url($logoStripeSrc[0]);
	$logoStripeMobile = get_field('home_mobile_logo_stripe');
	$logoStripeUrl = get_field('home_logo_stripe_url'); ?>

	<?php if ($logoStripeID) : ?>
		<div class="logo-stripe hidden-xs">
			<img class="attachment-logo-stripe size-logo-stripe" alt="home-logo-strip" src="<?php echo $logoStripeSrc[0]; ?>" srcset="<?php echo $logoStripeSrc[0]; ?>, <?php echo $logoStripeRetina; ?> 2x">
			<?php // echo wp_get_attachment_image($logoStripeID, 'logo-stripe'); ?>
			<?php if ($logoStripeUrl) : ?>
				<a href="<?php echo $logoStripeUrl; ?>" target="_blank"></a>
			<?php endif; ?>
		</div><!-- /.logo-stripe -->
	<?php endif; ?>

	<?php if ($logoStripeMobile) : ?>
		<div class="logo-stripe visible-xs" style="background-image:url('<?php echo $logoStripeMobile['url']; ?>');background-image:-webkit-image-set(url('<?php echo $logoStripeMobile['url']; ?>') 1x, url('https://www.829llc.com/wp-content/uploads/2016/10/logo-stripe-mobile-new@2x.png') 2x);">
			<?php if ($logoStripeUrl) : ?>
				<a href="<?php echo $logoStripeUrl; ?>" target="_blank"></a>
			<?php endif; ?>
		</div><!-- /.logo-stripe -->
	<?php endif; ?>

	<!-- =========== HOME CONTENT ========= -->

 <section class="page-content home">
 	<div class="container">

			<?php the_content(); ?>

 	</div><!-- /.container -->
 </section><!-- /.home-content -->

 <!-- =========== SLIDESHOW ========= -->

 <?php if (have_rows('home_add_slide')) : ?>

 	<section class="home-slideshow">
 		<div class="slide-list">
 			<?php while(have_rows('home_add_slide')) : the_row(); ?>
 			<?php 
 			if($is_iphone && get_sub_field('home_slide_mobile_image')) {
 				$slideBgUrl = get_sub_field('home_slide_mobile_image');
 			} else {
 				$slideBgUrl = get_sub_field('home_slide_image');
 			}
 			$clientName = get_sub_field('home_slide_client_name');
 			$shortDesc = get_sub_field('home_slide_short_description');
 			$slideUrl = get_sub_field('home_slide_url'); ?>
					<div<?php if($slideBgUrl) : ?> style="background-image: url('<?php echo $slideBgUrl; ?>');"<?php endif; ?>>
						<div class="slide-text-box">
							<?php if ($clientName) : ?>
								<h4><?php echo $clientName; ?></h4>
							<?php endif; ?>
							<?php if ($shortDesc) : ?>
								<p><?php echo $shortDesc; ?></p>
							<?php endif; ?>
							<?php if ($slideUrl) : ?>
								<a href="<?php echo $slideUrl; ?>" class="bt"><?php _e('Project Details', '829Studios'); ?></a>
							<?php endif ?>
						</div>
					</div>
 			<?php endwhile; ?>
 		</div><!-- /.slide-list -->
 	</section><!-- /.home-slideshow -->

 <?php endif; ?>

 <!-- =========== SERVICES ========= -->

 <?php
 $hideServices = get_field('hide_services');
 $servicesParentArgs = array('parent' => 0, 'hide_empty' => false);
		$parentServices = get_terms('service', $servicesParentArgs); ?>

	<?php if (!empty($parentServices) && !$hideServices) : ?>
	 <section id="services-content">
			<div class="services-page-content clearfix">
				<div class="page-content">
					<div class="content-wrapper">
						<?php $text = get_field('home_services_text'); ?>
						<?php echo $text; ?>
					</div><!-- /.content-wrapper -->
				</div><!-- /.page-content -->

				<?php $servicesParentArgs = array('parent' => 0, 'hide_empty' => false);
				$parentServices = get_terms('service', $servicesParentArgs); ?>

				<div class="services-box">
					<?php if (!empty($parentServices)) : ?>

						<?php foreach($parentServices as $service) : ?>

							<div class="single-service-box col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="single-service-wrapper">
									<div class="single-service-content">
										<?php $serviceId = $service->term_id; ?>
										<?php $serviceIcon = get_field('service_main_icon', 'service_'.$serviceId);
										$serviceIconIE = get_field('service_main_icon_ie', 'service_'.$serviceId);
										$name = $service->name; ?>
										<?php if ($serviceIcon) : ?>
											<object  data="<?php echo $serviceIcon['url'] ?>" type="image/svg+xml"><img src="<?php echo $serviceIcon['url'] ?>"></object>
										<?php endif; ?>
										<!--[if lt IE 9]>
											<?php if($serviceIconIE) : ?>
												<img src="<?php echo $serviceIconIE['url']; ?>" alt="<?php echo $serviceIconIE['alt']; ?>">
											<?php endif; ?>
											<!<![endif]-->
										<h6><?php echo $name; ?></h6>
										<div class="single-service-overlay">
											<a href="<?php echo get_term_link($service->slug, 'service'); ?>" class="service-link"></a>
											<div class="overlay-content">
												<h6><?php echo $name; ?></h6>
												<?php echo apply_filters("the_content", stripslashes($service->description)); ?>
											</div><!-- /.overlay-content -->
										</div><!-- /.single-service-overlay -->
									</div><!-- /.single-service-content -->
								</div>
							</div><!-- /.single-service-box -->

						<?php endforeach; ?>

					<?php endif; ?>
				</div><!-- /.services-box -->
			</div><!-- /.services-page-content -->
		</section><!-- /#services-content -->
	<?php endif; ?>

	<!-- =========== BOTTOM BOXES ========= -->

	<?php if (have_rows('home_bottom_boxes')) : ?>
		<section id="bottom-boxes">
			<?php while(have_rows('home_bottom_boxes')) : the_row(); ?>
			<?php $boxBgId = get_sub_field('bottom_box_background');
			$boxBg = wp_get_attachment_image($boxBgId, 'bottom-box');
			$boxTitle = get_sub_field('bottom_box_text');
			$boxLink = get_sub_field('bottom_box_link'); ?>
				<div class="bottom-box col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<?php if ($boxBg) : ?>
					<?php echo $boxBg; ?>
				<?php endif; ?>
					<?php if ($boxLink) : ?>
						<a href="<?php echo $boxLink; ?>" class="box-link"></a>
					<?php endif; ?>
					<?php if ($boxTitle) : ?>
						<div class="text-wrapper">
							<div class="inner">
								<h5><?php echo $boxTitle; ?></h5>
							</div>
						</div>
					<?php endif; ?>
				</div><!-- /.bottom-box -->
			<?php endwhile; ?>
		</section><!-- /#bottom-boxes -->
	<?php endif; ?>



<?php get_footer(); ?>
