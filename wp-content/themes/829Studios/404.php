<?php
/**
 * The 404 page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); ?>

<!-- =========== 404 ========= -->

<main class="single-page-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				 <h1><?php _e('404 - Not found.'); ?></h1>
				 <h2>
				  <?php _e("We're sorry, but the page you are looking for cannot be found. What should you do at this point? Here are some options:", "829Studios"); ?>
				 </h2>
				 <ul>
				  <li><?php _e("If you typed in a URL, check that it is typed in correctly.", "829Studios"); ?></li>
				  <li><?php _e("Perhaps it was just a fluke, and if you try again by clicking refresh, it'll pop right up!", "829Studios"); ?></li>
				  <li><?php _e("Or head back to our home page", "829Studios"); ?> <a href="<?php echo esc_url( home_url() ); ?>"><?php echo esc_url( home_url() ); ?></a> <?php _e("and navigate from there.", "829Studios"); ?> </li>
				 </ul>
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</main><!-- /.single-page-content -->

<?php get_footer(); ?>
