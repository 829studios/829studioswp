<?php

/**
 * Template Name: Careers Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header();
the_post(); ?>

<?php $bgVerticalPos = get_field('set_vertical_pos');
$bgHorizontalPos = get_field('set_horizontal_pos'); ?>

<!-- =========== CAREERS BANNER ========= -->

<?php if (have_rows('top_menu_sections')) : ?>
	<nav class="top-menu inner-nav">
		<ul>
			<?php $c = 1; ?>
			<?php while (have_rows('top_menu_sections')) : the_row(); ?>
				<?php $sectionTitle = get_sub_field('top_nav_section_title');
				$sectionLabel = get_sub_field('top_nav_section_label'); ?>
				<li><a href="#<?php echo $sectionLabel; ?>" <?php if ($c == 1) : ?> class="active" <?php endif; ?>><span><?php echo $sectionTitle; ?></span></a></li>
				<?php $c++; ?>
			<?php endwhile; ?>
		</ul>
		<a href="#" class="section-nav-prev top-menu-item"></a>
		<a href="#" class="section-nav-next top-menu-item"></a>
	</nav><!-- /.top-menu -->
<?php endif; ?>

<?php
$thumbId = get_post_thumbnail_id();
$thumbSrc = wp_get_attachment_image_src($thumbId, 'full');
$hideOpenings = get_field('hide_openings_btn');

?>

<section id="welcome" class="page-banner careers-banner" <?php if ($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if ($thumbId) : ?>background-image:url('<?php echo $thumbSrc[0]; ?>');<?php endif; ?>" <?php endif; ?>>
	<div class="page-banner-content">
		<h1 class="page-title animate-title"><?php the_title(); ?></h1>
		<?php if (!$hideOpenings) { ?>
			<div class="bt-wrapper animate-title">
				<a href="#openings" class="bt"><?php _e('View Openings', '829Studios'); ?></a>
			</div>
		<?php } ?>
	</div><!-- /.page-banner-content -->
</section><!-- /.page-banner -->

<!-- =========== CAREERS CONTENT ========= -->

<section id="careers-content" class="single-page-content">
	<div class="page-content careers">
		<div class="container">
			<?php the_content(); ?>
		</div><!-- /.container-fluid -->
	</div><!-- /.page-content -->

	<?php if (have_rows('careers_add_content')) : ?>
		<?php while (have_rows('careers_add_content')) : the_row(); ?>

			<?php if (get_row_layout() == 'careers_regular_content') : ?>

				<?php $secTitle = get_sub_field('reg_section_title');
				$secLabel = get_sub_field('reg_section_label');
				$content = get_sub_field('reg_content'); ?>

				<section id="<?php echo $secLabel; ?>" class="reg-content">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<?php if ($secTitle) : ?>
									<h2 class="section-title"><?php echo $secTitle; ?></h2>
								<?php endif; ?>
								<?php if ($content) : ?>
									<?php echo $content; ?>
								<?php endif; ?>
							</div><!-- /.col-lg-12 -->
						</div><!-- /.row -->
					</div><!-- /.container-fluid -->
				</section><!-- /.reg-content -->

			<?php elseif (get_row_layout() == 'carrers_full_width_gallery') : ?>

				<?php $secTitle = get_sub_field('gall_section_title');
				$secLabel = get_sub_field('gal_section_label');
				$text = get_sub_field('gall_text');
				$gallery = get_sub_field('gal_gallery_select'); ?>
				<section id="<?php echo $secLabel; ?>" class="full-width-gallery">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<?php if ($secTitle) : ?>
									<h2 class="section-title"><?php echo $secTitle; ?></h2>
								<?php endif; ?>
								<?php if ($text) : ?>
									<h4><?php echo $text; ?></h4>
								<?php endif; ?>
							</div><!-- /.col-lg-12 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
					<?php if ($gallery) : ?>
						<?php if (have_rows('add_gallery_slide', $gallery)) : ?>
							<div class="slider-wrapper">
								<div class="page-gallery">
									<?php while (have_rows('add_gallery_slide', $gallery)) : the_row(); ?>
										<?php $imgId = get_sub_field('gallery_slide_image');
										$imgSrc = wp_get_attachment_image_src($imgId, 'default-slider-size-full'); ?>
										<div style="background-image: url('<?php echo $imgSrc[0]; ?>');">

										</div>
									<?php endwhile; ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				</section><!-- /.full-width-gallery -->

			<?php elseif (get_row_layout() == 'benefits_content') : ?>

				<?php $secTitle = get_sub_field('ben_section_title');
				$secLabel = get_sub_field('ben_section_label');
				$text = get_sub_field('ben_text'); ?>

				<section id="<?php echo $secLabel; ?>" class="benefits">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<?php if ($secTitle) : ?>
									<h2 class="section-title"><?php echo $secTitle; ?></h2>
								<?php endif; ?>
								<?php if ($text) : ?>
									<h4><?php echo $text; ?></h4>
								<?php endif; ?>
							</div><!-- /.col-lg-12 -->
							<?php if (have_rows('ben_add_perk')) : ?>
								<div class="box-info-wrapper clearfix">
									<?php while (have_rows('ben_add_perk')) : the_row(); ?>
										<div class="single-box-info col-lg-4 col-md-4 col-sm-6 col-xs-12">
											<?php $perkTitle = get_sub_field('perk_title');
											$perkText = get_sub_field('perk_text');
											$perkImg = get_sub_field('perk_image'); ?>
											<?php if ($perkImg) : ?>
												<img src="<?php echo $perkImg['url']; ?>" alt="<?php echo $perkImg['alt']; ?>">
											<?php endif ?>
											<?php if ($perkTitle) : ?>
												<h6><?php echo $perkTitle; ?></h6>
											<?php endif; ?>
											<?php if ($perkText) : ?>
												<p><?php echo $perkText; ?></p>
											<?php endif; ?>
										</div><!-- /.single-box-info -->
									<?php endwhile; ?>
								</div><!-- /.box-info-wrapper -->
							<?php endif; ?>
						</div><!-- /.row -->
					</div><!-- /.container -->
				</section><!-- /.benefits -->

			<?php elseif (get_row_layout() == 'quote_content') : ?>

				<?php $sectionBg = get_sub_field('quote_background');
				$quoteText = get_sub_field('quote_text');
				$quoteAuthor = get_sub_field('quote_author'); ?>

				<section class="quote" <?php if ($sectionBg) : ?> style="background-image: url('<?php echo $sectionBg; ?>');" <?php endif; ?>>
					<div class="quote-wrapper">
						<div class="container">
							<?php if ($quoteText) : ?>
								<h4><?php echo $quoteText; ?></h4>
							<?php endif; ?>
							<?php if ($quoteAuthor) : ?>
								<p><?php echo $quoteAuthor; ?></p>
							<?php endif; ?>
						</div><!-- /.container -->
					</div><!-- /.quote-wrapper -->
				</section><!-- /.quote -->

			<?php elseif (get_row_layout() == 'openings_content') : ?>

				<?php $secTitle = get_sub_field('open_setction_title');
				$secLabel = get_sub_field('open_section_label');
				$text = get_sub_field('open_text'); ?>

				<section id="<?php echo $secLabel; ?>" class="jobs">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<?php if ($secTitle) : ?>
									<h2 class="section-title"><?php echo $secTitle; ?></h2>
								<?php endif; ?>
								<?php if ($text) : ?>
									<h4><?php echo $text; ?></h4>
								<?php endif; ?>
								<?php if (have_rows('add_job')) : ?>
									<div class="jobs-wrapper">
										<?php while (have_rows('add_job')) : the_row(); ?>
											<?php $jobTitle = get_sub_field('job_title');
											$jobExcerpt = get_sub_field('job_excerpt');
											$jobImage = get_sub_field('job_image');
											$jobContent = get_sub_field('job_content');
											$buttonLabel = get_sub_field('job_button_label');
											$buttonUrl = get_sub_field('job_button_url'); ?>
											<div class="accordion">
												<div class="accordion-header">
													<?php if ($jobImage) : ?>
														<img src="<?php echo $jobImage['url']; ?>" alt="<?php echo $jobImage['alt']; ?>">
													<?php endif; ?>
													<h5><?php echo $jobTitle; ?></h5>
													<?php if ($jobExcerpt) : ?>
														<p><?php echo $jobExcerpt; ?></p>
													<?php endif; ?>
												</div>
												<div class="accordion-content">
													<?php echo $jobContent; ?>
													<?php if ($buttonUrl && $buttonLabel) : ?>
														<a href="<?php echo $buttonUrl; ?>" class="bt color2" target="_blank"><?php echo $buttonLabel; ?></a>
													<?php endif; ?>
												</div>
											</div>
										<?php endwhile; ?>
									</div><!-- /.jobs-wrapper -->
								<?php endif; ?>
							</div><!-- /.col-lg-12 -->
						</div><!-- /.row -->
						<?php $bottomText = get_sub_field('open_bottom_text'); ?>
						<?php if ($bottomText) : ?>
							<div class="bottom-text"><?php echo $bottomText; ?></div>
						<?php endif; ?>
					</div><!-- /.container -->
				</section><!-- /.jobs -->


			<?php endif; ?>

		<?php endwhile; ?>
	<?php endif; ?>


</section><!-- /#careers-content -->



<?php get_footer(); ?>