<?php
/**
 * Loop for home page.
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */
?>
<section class="posts">
 <?php while (have_posts()) : the_post(); ?>
		<article class="single-post">
			<div class="container">
				<div class="row">
						<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php if (has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumb'); ?></a>
						<?php endif; ?>
						<?php $text = get_the_excerpt(); ?>
						<?php if ($text) : ?>
							<p><?php echo stripteaser($text, 150); ?></p>
						<?php endif; ?>
						<?php $authorId = get_the_author_meta( 'ID'); ?>
						<?php $authorPhoto = get_field('user_photo','user_'.$authorId); ?>
						<div class="post-meta">
							<span class="author"><?php the_author_posts_link(); ?></span>
							<?php if ($authorPhoto) : ?>
							<?php echo wp_get_attachment_image( $authorPhoto ,'author-thumb', false, array('class' => 'author-photo')); ?>	
							<?php endif; ?>
							<span class="date"><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m'));?>"><?php echo get_the_date(get_option('date_format')); ?></a></span>
						</div><!-- /.post-meta -->
				</div><!-- .row -->
			</div><!-- .container -->
		</article><!-- /.single-post -->
 <?php endwhile; ?>
</section><!-- /.posts -->
