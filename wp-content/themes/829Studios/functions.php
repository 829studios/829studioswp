<?php
//**************************************************************************** */
//
//  1. Init
//  2. Navigation
//  3. Widget Areas
//  4. Thumbnails
//  5. Core functions
//  6. Custom functions
//
//**************************************************************************** */

function get_settings_json() {

	$url = get_template_directory() . '/settings.json';
	$json = file_get_contents($url);
	$json = preg_replace("/\/\*(?s).*?\*\//", "", $json);
	$json = json_decode($json);
	return $json;
}

$json = get_settings_json();

/* * ******************************** Init *********************************** */

include('lib/init.php');

/* * ****************************** Navigation ******************************* */

include('lib/nav.php');

/* * ****************************** Widget Areas ***************************** */

include('lib/widget-areas.php');

/* * ****************************** Thumbnails ******************************* */

include('lib/thumbnails.php');


/* * ************************* Core Functions ******************************** */

include('lib/core-functions.php');

/* * ************************ Custom Functions ******************************* */

include('lib/custom-functions.php');
