<?php $bgCol = get_sub_field('section_background_color');
	$bgSrc = get_sub_field('section_background_image');
	$bgVerticalPos = strtolower(get_sub_field('section_background_image_vertical_alignment'));
	$bgHorizontalPos = strtolower(get_sub_field('section_background_image_horizontal_alignment'));
	$bgType = get_sub_field('section_background_image_type');
	$content = get_sub_field('section_content'); ?>

<section class="section full-section text-section <?php echo $bgType; ?>" style="background-color: <?php echo $bgCol; ?>; background-image: url(<?php echo $bgSrc; ?>); background-position: <?php echo $bgVerticalPos . ' ' . $bgHorizontalPos; ?>;">
	<div class="section-content">
		<div class="inner">
			<?php echo $content; ?>
		</div>

	</div>
</section>
