<?php $bgCol = get_sub_field('section_background_color');
	$bgSrc = get_sub_field('section_background_image');
	$bgVerticalPos = strtolower(get_sub_field('section_background_image_vertical_alignment'));
	$bgHorizontalPos = strtolower(get_sub_field('section_background_image_horizontal_alignment'));
	$bgType = get_sub_field('section_background_image_type');
	$mainImg = get_sub_field('image'); ?>

<section class="section full-section single-image <?php echo $bgType; ?>" style="background-color: <?php echo $bgCol; ?>; background-image: url(<?php echo $bgSrc; ?>); background-position: <?php echo $bgVerticalPos . ' ' . $bgHorizontalPos; ?>;">
	<div class="section-content">
		<div class="inner">
			<?php if( $mainImg ) {
				echo wp_get_attachment_image( $mainImg, 'full' );
			} ?>
		</div>

	</div>
</section>
