
<section class="section full-section sign-off">
	<div class="section-content">
		<div class="inner">
			<header>
				<h2><?php the_sub_field('section_title'); ?></h2>
			</header>
			<div class="content-wrapper">
				<a class="bt" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a>
			</div>
		</div>
	</div>
</section>
