
<section class="section full-section results">
	<div class="section-content">
		<div class="inner">
			<header>
				<h2><?php the_sub_field('section_title'); ?></h2>
			</header>
			<?php if (have_rows('results')): ?>
				<ul>
					<?php while (have_rows('results')): the_row(); ?>
						<li>
							<?php $mainImg = get_sub_field('result_image');
								$subtitle = get_sub_field('result_statement'); ?>
							<?php if( $mainImg ) {
								echo wp_get_attachment_image( $mainImg, 'full' );
							} ?>
							<h5><?php echo $subtitle; ?></h5>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>
</section>
