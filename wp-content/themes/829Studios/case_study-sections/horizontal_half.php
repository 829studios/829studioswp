<?php $bgCol = get_sub_field('section_background_color');
	$bgSrc = get_sub_field('section_background_image');
	$bgVerticalPos = strtolower(get_sub_field('section_background_image_vertical_alignment'));
	$bgHorizontalPos = strtolower(get_sub_field('section_background_image_horizontal_alignment'));
	$bgType = get_sub_field('section_background_image_type'); ?>

<section class="section full-section horizontal-half <?php echo $bgType; ?>" style="background-color: <?php echo $bgCol; ?>; background-image: url(<?php echo $bgSrc; ?>); background-position: <?php echo $bgVerticalPos . ' ' . $bgHorizontalPos; ?>;">
	<div class="section-content">
		<?php $content = get_sub_field('left_side_content');
		$leftBgCol = get_sub_field('left_background_color');
		$leftBgSrc = get_sub_field('left_background_image');
		$leftBgHor = strtolower(get_sub_field('left_background_image_horizontal_alignment'));
		$leftBgVer = strtolower(get_sub_field('left_background_image_vertical_alignment'));
		$leftBgType = get_sub_field('left_background_image_type');

		$mainImg = get_sub_field('main_image');
		$mobileImg = get_sub_field('mobile_image');
		$mobileImgVer = strtolower(get_sub_field('mobile_image_vertical_alignment'));
		$mainImgType = strtolower(get_sub_field('image_type'));
		$rightBgCol = get_sub_field('right_background_color');
		$rightBgSrc = get_sub_field('right_background_image');
		$rightBgHor = strtolower(get_sub_field('right_background_image_horizontal_alignment'));
		$rightBgVer = strtolower(get_sub_field('right_background_image_vertical_alignment'));
		$rightBgType = get_sub_field('right_background_image_type');
		 ?>
	    <div class="half content-half <?php echo $leftBgType; ?>" style="background-color: <?php echo $leftBgCol; ?>; background-image: url(<?php echo $leftBgSrc; ?>); background-position: <?php echo $leftBgVer . ' ' . $leftBgHor; ?>;">
	    	<div class="content">
	    		<div class="inner">
	    			<?php echo $content; ?>
	    		</div>
	    	</div>
	    </div>
	    <div class="half image-half <?php echo $rightBgType; ?>" style="background-color: <?php echo $rightBgCol; ?>; background-image: url(<?php echo $rightBgSrc; ?>); background-position: <?php echo $rightBgVer . ' ' . $rightBgHor; ?>;">
	    	<div class="image-wrapper image-desktop <?php echo $mainImgType; ?>">
				<div class="inner">
					<?php if( $mainImg ) {
						echo wp_get_attachment_image( $mainImg, 'full' );
					} ?>
				</div>
	    	</div>
	    	<div class="image-wrapper image-mobile <?php echo $mainImgType . ' ' . $mobileImgVer; ?>">
				<div class="inner">
					<?php if( $mobileImg ) {
						echo wp_get_attachment_image( $mobileImg, 'full' );
					} ?>
				</div>
	    	</div>
	    </div>

	    <pre style="display: none;">
	    	<?php $test = get_sub_field('right_section_hidden_mobile');
	    	var_dump($test); ?>
	    </pre>

	</div>
</section>
