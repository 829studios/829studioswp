
<?php $imgSrc = get_sub_field('full_image_image');
	$bgVerticalPos = strtolower(get_sub_field('section_background_image_vertical_alignment'));
	$bgHorizontalPos = strtolower(get_sub_field('section_background_image_horizontal_alignment')); ?>

<section class="section full-section full-image" style="background-image: url(<?php echo $imgSrc; ?>); background-position: <?php echo $bgVerticalPos . ' ' . $bgHorizontalPos; ?>;">
</section>
