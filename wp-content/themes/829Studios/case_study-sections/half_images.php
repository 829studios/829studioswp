<?php $leftSrc = get_sub_field('left_image');
	$leftVerticalPos = strtolower(get_sub_field('left_image_vertical_alignment'));
	$leftHorizontalPos = strtolower(get_sub_field('left_image_horizontal_alignment'));
	$rightSrc = get_sub_field('right_image');
	$rightVerticalPos = strtolower(get_sub_field('right_image_vertical_alignment'));
	$rightHorizontalPos = strtolower(get_sub_field('right_image_horizontal_alignment')); ?>

<section class="section full-section half-images">
	<div class="section-content">
	    <div class="half image-half" style="background-image: url(<?php echo $leftSrc; ?>); background-position: <?php echo $leftVerticalPos . ' ' . $leftHorizontalPos; ?>;">
	    </div>
	    <div class="half image-half" style="background-image: url(<?php echo $rightSrc; ?>); background-position: <?php echo $rightVerticalPos . ' ' . $rightHorizontalPos; ?>;">
	    </div>

	</div>
</section>
