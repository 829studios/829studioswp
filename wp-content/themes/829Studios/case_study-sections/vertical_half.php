<?php $bgCol = get_sub_field('section_background_color');
	$bgSrc = get_sub_field('section_background_image');
	$bgVerticalPos = strtolower(get_sub_field('section_background_image_vertical_alignment'));
	$bgHorizontalPos = strtolower(get_sub_field('section_background_image_horizontal_alignment'));
	$bgType = get_sub_field('section_background_image_type'); ?>

<?php $altImg = get_sub_field('alt_image');
	$altImgType = strtolower(get_sub_field('alt_image_type'));
	if ($altImgType == "floating") {
		if (get_sub_field('alt_horizontal_alignment')) {
			$altImgHor = get_sub_field('alt_horizontal_alignment');
		}
		if (get_sub_field('alt_vertical_alignment')) {
			$altImgVer = get_sub_field('alt_vertical_alignment');
		}
		if (get_sub_field('alt_cropping')) {
			$altCrop = get_sub_field('alt_cropping');
		}
		if (get_sub_field('alt_padding_bottom')) {
			$altPadBottom = get_sub_field('alt_padding_bottom');
		}
	} ?>


<section class="section full-section vertical-half <?php echo $bgType; ?> <?php if ($altImg) { echo 'alt-img'; } ?>" style="background-color: <?php echo $bgCol; ?>; background-image: url(<?php echo $bgSrc; ?>); background-position: <?php echo $bgVerticalPos . ' ' . $bgHorizontalPos; ?>;">
	<div class="section-content">
		<?php $content = get_sub_field('left_side_content');
		$leftBgCol = get_sub_field('left_background_color');
		$leftBgSrc = get_sub_field('left_background_image');
		$leftBgHor = strtolower(get_sub_field('left_background_image_horizontal_alignment'));
		$leftBgVer = strtolower(get_sub_field('left_background_image_vertical_alignment'));
		$leftBgType = get_sub_field('left_background_image_type');

		$mainImg = get_sub_field('main_image');
		$mainImgType = strtolower(get_sub_field('image_type'));
		if ($mainImgType == "floating") {
			if (get_sub_field('horizontal_alignment')) {
				$mainImgHor = get_sub_field('horizontal_alignment');
			}
			if (get_sub_field('vertical_alignment')) {
				$mainImgVer = get_sub_field('vertical_alignment');
			}
		}
		$rightBgCol = get_sub_field('right_background_color');
		$rightBgSrc = get_sub_field('right_background_image');
		$rightBgHor = strtolower(get_sub_field('right_background_image_horizontal_alignment'));
		$rightBgVer = strtolower(get_sub_field('right_background_image_vertical_alignment'));
		$rightBgType = get_sub_field('right_background_image_type');
		 ?>
	    <div class="half content-half <?php echo $leftBgType; ?>" style="background-color: <?php echo $leftBgCol; ?>; background-image: url(<?php echo $leftBgSrc; ?>); background-position: <?php echo $leftBgVer . ' ' . $leftBgHor; ?>;">
	    	<div class="content">
	    		<div class="inner">
	    			<?php echo $content; ?>
	    		</div>
	    	</div>
	    </div>
	    <div class="half image-half <?php echo $rightBgType; ?> <?php if(get_sub_field('right_section_hidden')): ?> mobile-right-hidden <?php endif; ?>" style="background-color: <?php echo $rightBgCol; ?>; background-image: url(<?php echo $rightBgSrc; ?>); background-position: <?php echo $rightBgVer . ' ' . $rightBgHor; ?>;">
	    	<div class="image-wrapper main <?php echo $mainImgType . ' ' . $mainImgHor . ' ' . $mainImgVer; ?>">
				<div class="inner">
					<?php if( $mainImg ) {
						echo wp_get_attachment_image( $mainImg, 'full' );
					} ?>
				</div>
	    	</div>

	    	<div class="image-wrapper alt <?php echo $altImgType . ' ' . $altImgHor . ' ' . $altImgVer . ' ' . $altCrop . ' ' . $altPadBottom; ?>">
				<div class="inner">
					<?php if( $altImg ) {
						echo wp_get_attachment_image( $altImg, 'full' );
					} ?>
				</div>
	    	</div>
	    </div>

	</div>
</section>
