<?php
/**
 * The footer for theme.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */
?>

 </section><!-- /.main -->

  <!-- =========== FOOTER ========= -->

  <?php $phone = get_field('footer_phone', 'option');
  $address = get_field('footer_address', 'option');
  $btLabel = get_field('footer_button_label', 'option');
  $btUrl = get_field('footer_button_url', 'option');
  $footnote = get_field('footnote', 'option');
  $fb = get_field('facebook_link', 'option');
  $tw = get_field('twitter_link', 'option');
  $linked = get_field('linkedin_link', 'option');
  $inst = get_field('instagram_link', 'option');
  $drib = get_field('dribbble_link', 'option'); ?>

  <footer class="main">
  	<div class="footer-top clearfix">
  		<?php if ($btLabel && $btUrl) : ?>
  			<a href="<?php echo $btUrl; ?>" class="bt"><?php echo $btLabel; ?></a>
  		<?php endif; ?>
  		<a href="<?php bloginfo('url'); ?>" class="footer-logo"></a>
  		<?php if ($phone) : ?>
  			<a href="tel:<?php echo $phone; ?>" class="phone"><?php echo $phone; ?></a>
  		<?php endif; ?>
  		<?php if ($address) : ?>
  			<p class="address"><?php echo $address; ?></p>
  		<?php endif; ?>
  		<ul class="social-list list-mobile">
   			<?php if ($fb) : ?>
   				<li class="fb"><a href="<?php echo $fb; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($tw) : ?>
   				<li class="tw"><a href="<?php echo $tw; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($linked) : ?>
   				<li class="linked"><a href="<?php echo $linked; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($inst) : ?>
   				<li class="inst"><a href="<?php echo $inst; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($drib) : ?>
   				<li class="drib"><a href="<?php echo $drib; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   		</ul><!-- /.social-list -->
  	</div><!-- /.footer-top -->
   <div class="footer-bottom clearfix">
   	<?php if ($footnote) : ?>
   		<p class="footnote"><?php echo $footnote; ?></p>
   		<ul class="social-list">
   			<?php if ($fb) : ?>
   				<li class="fb"><a href="<?php echo $fb; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($tw) : ?>
   				<li class="tw"><a href="<?php echo $tw; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($linked) : ?>
   				<li class="linked"><a href="<?php echo $linked; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($inst) : ?>
   				<li class="inst"><a href="<?php echo $inst; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   			<?php if ($drib) : ?>
   				<li class="drib"><a href="<?php echo $drib; ?>" target="_blank"></a></li>
   			<?php endif; ?>
   		</ul><!-- /.social-list -->
   	<?php endif; ?>
   </div><!-- /.footer-bottom -->
  </footer><!-- /.main -->
  <!-- 
<div class="page-overlay-loader">
    <div class="loader-icons-wrap">
      <div class="loader-ico-wrap">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/loader.svg" alt="829 studios" onerror="this.src='<?php echo get_bloginfo('template_directory'); ?>/images/loader.png'">
      </div>
      <div class="loader-ico-wrap fill-wrap">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/loader.svg" alt="829 studios" onerror="this.src='<?php echo get_bloginfo('template_directory'); ?>/images/loader.png'">
      </div>
    </div>
  </div>
 -->
 </section><!-- /#page -->
 <div class="page-overlay-loader" id="page-overlay" style="display:none;">
    <div class="loader-icons-wrap">
      <div class="loader-ico-wrap">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/loader.svg" alt="829 studios" onerror="this.src='<?php echo get_bloginfo('template_directory'); ?>/images/loader.png'">
      </div>
      <div class="loader-ico-wrap fill-wrap">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/loader.svg" alt="829 studios" onerror="this.src='<?php echo get_bloginfo('template_directory'); ?>/images/loader.png'">
      </div>
    </div>
  </div>
  <script type="text/javascript">
		if (!sessionStorage.firstLoad) {
			jQuery( function() {
			
				document.getElementById('page').style.visibility = 'hidden'; // show
				var pageLoader = jQuery('.page-overlay-loader');
				pageLoader.fadeIn(400, function(){
					var loaderIco = jQuery('.page-overlay-loader .loader-ico-wrap.fill-wrap');
					jQuery('.page-overlay-loader .loader-ico-wrap.fill-wrap').toggleClass('active');
					loaderInterval = setInterval(function(){
						jQuery('.page-overlay-loader .loader-ico-wrap.fill-wrap').toggleClass('active');
					}, 2000);
					document.getElementById('page').style.visibility = 'visible'; // show
				});
				setTimeout(function() {
					jQuery.event.trigger({
						type: "pageLoaderEvent",
						message: "The page loader is done.",
						time: new Date()
					});
					pageLoader.addClass('loaded');
				}, 2000);
			});
		} else {
			document.getElementById('page').style.visibility = 'visible'; // show
		}
	</script>
<?php wp_footer(); ?>


</body>
</html>
