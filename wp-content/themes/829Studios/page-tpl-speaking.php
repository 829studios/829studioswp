<?php
/**
 * Template Name: Speaking Engagement Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $bgVerticalPos = get_field('set_vertical_pos');
$bgHorizontalPos = get_field('set_horizontal_pos'); ?>

<!-- =========== SPEAKING ENGAGEMENT BANNER ========= -->

<?php $thumbId = get_post_thumbnail_id();
$thumbSrc = wp_get_attachment_image_src($thumbId, 'work-thumb'); ?>

	<section class="page-banner contact-banner"<?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($thumbId) : ?>background-image:url('<?php echo $thumbSrc[0]; ?>');<?php endif; ?>"<?php endif; ?>>
		<div class="page-banner-content">
			<h1 class="page-title animate-title"><?php the_title(); ?></h1>
		</div><!-- /.page-banner-content -->
	</section><!-- /.page-banner -->

<!-- =========== SPEAKING ENGAGEMENT CONTENT ========= -->

<section id="contact-content" class="single-page-content">
	<?php 
    $aboutUs = get_field('about_us');
	$speakers = get_field('speakers');
	$projects = get_field('recent_projects'); 
    $sessions = get_field('speaking_sessions'); ?>

	<div class="container">
		<div class="row">
            <?php if ($aboutUs) : ?>
                <div class="col-lg-12">
                    <?php echo $aboutUs; ?>
                </div>
            <?php endif; ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
	
	
	
	
	
<?php if( have_rows('speakers') ): ?>
    <div class="container">
        <div class="row">

            <div class="speaker-wrapper">
            <?php 
            $counter = 1;
            // loop through rows (parent repeater)
            while( have_rows('speakers') ): the_row(); 
                $name = get_sub_field('speaker_name');
                $image = get_sub_field('speaker_image');
                $bio = get_sub_field('speaker_lightbox_bio');
                
                ?>
                
               <div class="speaker-box col-md-3 col-sm-6 col-xs-12">
                    <img src="<?php echo $image['url'];  ?>">	
                    <div class="text-box">
                        <h4><?php echo $name; ?></h4>
                        <a class="bt" data-toggle="modal" data-target="#speaker-<?php echo $counter; ?>">Read Bio</a>
                    </div>
                </div>

                <div id="speaker-<?php echo $counter; ?>" class="modal fade speaker-info" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header"><button class="close" type="button" data-dismiss="modal" aria-label="close">×</button></div>
                        <div class="modal-body">
                            <div class="row">
                              <div class="col-sm-4 col-xs-12">
                                  <img src="<?php echo $image['url'];  ?>">
                              </div>
                              <div class="col-sm-8 col-xs-12">
                                  <h4><?php echo $name; ?></h4>
                                  <div><?php echo $bio; ?></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                
             <?php $counter++;
             endwhile; // while( has_sub_field('to-do_lists') ): ?>
            </div>

        </div><!-- /.row -->
    </div><!-- /.container -->
<?php endif; // if( get_field('to-do_lists') ): ?>
		
		

    
	
	<div class="container">
		<div class="row">
            <?php if ($projects) : ?>
                <div class="col-lg-12">
                    <?php echo $projects; ?>
                </div>
            <?php endif; ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
	
	<div class="container">
		<div class="row">
            <?php if ($sessions) : ?>
                <div class="col-lg-12">
                    <?php echo $sessions; ?>
                </div>
            <?php endif; ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /#contact-content -->

<?php get_footer(); ?>
