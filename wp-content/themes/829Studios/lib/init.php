<?php
$json = get_settings_json();

/* * ******************************** Init ************************************* */

$path = (dirname(__FILE__)).'/widgets/';
foreach (glob($path.'*.php') as $filename) {
	if(basename($filename)=="index.php")
	include_once($filename);
}


/* Add Theme support */

add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
add_theme_support( 'title-tag' );

/* Add theme options page by default */

if( (!isset($json->acf_options)) || ( $json->acf_options->init == true) ){
	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
			'page_title'    => $json->acf_options->page_title,
			'menu_title'    => $json->acf_options->menu_title,
			'menu_slug'     => $json->acf_options->menu_slug
		));
		if( isset($json->acf_options->subpages) ){
			foreach( $json->acf_options->subpages as $subpage ){
				acf_add_options_sub_page(array(
					'page_title'    => $subpage->page_title,
					'menu_title'    => $subpage->menu_title,
					'parent_slug'   => $json->acf_options->menu_slug,
				));
			}
		}
	}
}

if( (!isset($json->localizeScripts)) || ( $json->localizeScripts == true) ){ // localize scripts by default
	add_filter('wp_footer', 'localize_scripts');
}

if( (!isset($json->widget_text)) || ( $json->widget_text == true) ){ // enable text widget by default
	add_filter('widget_text', 'do_shortcode');
}

if( (!isset($json->pageExcerpt)) || ( $json->pageExcerpt == true) ){ // add excerpt field to pages by default
	add_post_type_support( 'page', 'excerpt' );
}

/* Enqueue styles and scripts */

add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
function theme_enqueue_scripts(){

	//scripts
	wp_enqueue_script("jquery");
	if(is_singular() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	wp_enqueue_script('bootstrap', "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js", null, '1.0', true);
	wp_enqueue_script('slick', "//cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js", null, '1.0', true);
	wp_enqueue_script('mCustomScroll', "//cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.8/jquery.mCustomScrollbar.concat.min.js", null, '1.0', true);
	wp_enqueue_script('ezmark', get_template_directory_uri()."/js/jquery.ezmark.min.js", null, '1.0', true);
	wp_enqueue_script('scrollTo', get_template_directory_uri()."/js/jquery.scrollTo.min.js", null, '5.0', true);

	wp_enqueue_script('localScroll', get_template_directory_uri()."/js/jquery.localScroll.min.js", null, '6.0', true);

	wp_enqueue_script('pagePiling', get_template_directory_uri()."/js/vendor/jquery.fullPage.min.js", null, '1.0', true);
	wp_enqueue_script('functions', get_template_directory_uri()."/js/functions.js", null, '1.0', true);

	//styles

	wp_enqueue_style('font-Merriweather', "//fonts.googleapis.com/css?family=Merriweather:400,400italic", null, '1.0', 'all');
	wp_enqueue_style('bootstrap', "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css", null, '1.0', 'all');
	wp_enqueue_style('slick', "//cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css", null, '1.0', 'all');
 wp_enqueue_style('mCustomScroll', "//cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.8/jquery.mCustomScrollbar.min.css", null, '1.0', 'all');
 wp_enqueue_style('pagePilingStyles',  get_template_directory_uri()."/css/vendor/jquery.fullPage.css", null, '1.0', 'all');
	wp_enqueue_style('style', get_template_directory_uri()."/style.css", null, '1.0', 'all');

}



/* * ***************************** Amin UI ************************************* */

if( (!isset($json->manage_posts_columns)) || ( $json->manage_posts_columns == true) ){ // add columns to post list in admin panel by default

	add_filter('manage_posts_columns', 'posts_columns_id', 5);
	add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
	add_filter('manage_pages_columns', 'posts_columns_id', 5);
	add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);
	add_filter('manage_media_columns', 'column_id');
	add_filter('manage_media_custom_column', 'column_id_row', 10, 2);

	function posts_columns_id($defaults) {
		$defaults['wps_post_id'] = __('ID');
		return $defaults;
	}

	function posts_custom_id_columns($column_name, $id) {
		if ($column_name === 'wps_post_id') {
		  echo $id;
		}
	}

	function column_id($columns) {
		$columns['colID'] = __('ID');
		return $columns;
	}

	function column_id_row($columnName, $columnID) {
		if ($columnName == 'colID') {
		  echo $columnID;
		}
	}

}

/* * *************************** TGM ACtivation Plugin ********************** */
require_once dirname( __FILE__ ) . '/plugins/TGM-Plugin-Activation/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'defualt_register_required_plugins' );

function defualt_register_required_plugins() {

    $plugins = array(

        array(
            'name'               => 'Custom Post Types, Taxonomies and Shortcodes', // The plugin name.
            'slug'               => 'cptts', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/lib/plugins/cptts.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '0.5', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        )

    );

    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
            'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}

?>
