<?php

function add_autoplay( $embed )
{
	$findsrc = preg_match('/src="([^"]+)/', $embed, $matches);
 
	if( $findsrc === 1 ) {
		$src = add_query_arg('autoplay', 1, $matches[1]);
		return preg_replace('/src="([^"]+)/', 'src="' . $src, $embed);
	} else {
		return $embed;
	}
}

function get_ajax_posts(){

	global $post;

	$offset = $_POST['offset'];
	$category = $_POST['category'];

	$queryArgs = array(
		'post_type' => 'post',
		'order' => 'DESC',
		'orderby' => 'date',
		'offset' => $offset,
		'post_status' => 'publish'
		);

	if ($category) {
		$queryArgs['category_name'] = $category;
	}

		$news = new WP_Query($queryArgs); ?>

		<?php if($news->have_posts()) : ?>
		<?php while($news->have_posts()) : $news->the_post(); ?>
		<?php

		$authorId = get_the_author_meta( 'ID' );
		$authorPhotoId = get_field('user_photo', 'user_'. $authorId);
		$authorPhotoSrc = wp_get_attachment_image_src($authorPhotoId, 'user-photo');
		$authorPhotoAlt = get_post_meta($authorPhotoId, '_wp_attachment_image_alt', true);
		$url = get_permalink();
		$thumbId = get_post_thumbnail_id();
		$thumbSrc = wp_get_attachment_image_src($thumbId, 'post-list-thumb');
		$thumbAlt = get_post_meta($thumbId, '_wp_attachment_image_alt', true);
		$title = get_the_title();
		$text =  get_the_excerpt();
		$authorLink = get_author_posts_url($authorId);
		$authorName = get_the_author_meta('display_name', $authorId);
		$date = get_the_date(get_option('date_format'));
		$monthLink = get_month_link(get_the_time('Y'), get_the_time('m'));

		$post = '<article class="single-post">';

		$post .= '<div class="container">';

		$post .= '<div class="row">';

		$post .= '<h2 class="post-title"><a href="'.$url.'">'.$title.'</a></h2>';

		if (!empty($thumbId)) {

			$post .= '<a href="'.$url.'"><img src="'.$thumbSrc[0].'" alt="'.$thumbAlt.'"></a>';

		}

		$post .= '<p>'.stripteaser($text, 150).'</p>';

		$post .= '<div class="post-meta">';

		$post .= '<span class="author"><a href="'.$authorLink.'">'.$authorName.'</a></span>';

		if ($authorId) {
			$post .= '<span class="author-photo" style="background-image: url('.$authorPhotoSrc[0].');"></span>';
		}

		$post .= '<span class="date"><a href="'.$monthLink.'">'.$date.'</a></span>';

		$post .= '</div></div></div></article>';

		 ?>

		<?php $allPosts[] = $post;?>
		<?php endwhile; ?>
		<?php endif; wp_reset_query(); ?>

		<?php echo json_encode($allPosts); ?>

		<?php die(); ?>

<?php }

add_action('wp_ajax_get_ajax_posts', 'get_ajax_posts');
add_action('wp_ajax_nopriv_get_ajax_posts', 'get_ajax_posts' );

function remove_empty_paragraphs($sContent) {
 $sContent = str_replace("]</p>", "]", $sContent);
 $sContent = str_replace("\>\<\/p\>", "\>", $sContent);
 $sContent = str_replace("<p>[", "[", $sContent);
 return $sContent;
}

add_filter('the_content', 'remove_empty_paragraphs');

//Allow SVG through WordPress Media Uploader

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Fix SVG in WordPress Admin Area
function custom_admin_head() {
    $css = '';

    $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';

    echo '<style type="text/css">'.$css.'</style>';
}
add_action('admin_head', 'custom_admin_head');

function get_svg($id) {
	$fileSVG = wp_get_attachment_image_src($id, 'full');

	if(!empty($fileSVG)):
		$section = @file_get_contents($fileSVG[0]);
		if($section){
			$findme   = '<svg';
			$pos = strpos($section, $findme);
			$svg = substr($section, $pos);
			return $svg;
		}

		return '';
	endif;
}

add_action('admin_head', 'custom_styles');

function custom_styles() {
  echo '<style>
    .clear-left {
      clear: left !important;
    }
  </style>';
}


function lightbox_ajax() {
	$id = $_POST['lightboxid'];
	if(have_rows('lightbox_sections', $id)) : ?>
		<header>
			<h6><?php echo get_the_title($id); ?></h6>
			<a href="#" class="close"></a>
		</header>
		<div class="lightbox-content">
			<div class="lightbox-slider">
				<?php while(have_rows('lightbox_sections', $id)) : the_row(); ?>
					<?php if(get_row_layout() == "rich_text"): // layout: Content ?>

						<div class="slide text">
							<div class="slide-container">
								<div class="slide-wrapper">
									<div class="text-slide">
										<?php the_sub_field("content"); ?>
									</div>
								</div>
							</div>
						</div>

					<?php elseif(get_row_layout() == "video"): // layout: Content ?>

						<div class="slide video">
							<div class="slide-container">
								<div class="slide-wrapper">
									<div class="video-slide">
										<?php $ratio = get_sub_field('video_ratio');
											$vidSrc = get_sub_field('video_url');
										if ($ratio = "16:9") {
										 	$r = "16by9";
										 } else {
										 	$r = "4by3";
										 } ?>

										<?php $query = new WP_Query(['posts_per_page' => 1]);
										$query->the_post(); ?>

										<div class="embed-responsive embed-responsive-<?php echo $r; ?>">
											<?php 

												$my_embed = $GLOBALS['wp_embed']->shortcode(array(), $vidSrc);
												echo add_autoplay($my_embed);
											?>
										</div>

										<?php wp_reset_query(); ?>
									</div>
								</div>
							</div>
						</div>

					<?php elseif(get_row_layout() == "image"): ?>
						<?php $imgSrc = get_sub_field('image_src');
							$imgType = strtolower(get_sub_field('image_type')); ?>
						<div class="slide image <?php echo $imgType; ?>">

							<div class="slide-container">
								<div class="slide-wrapper">
									<div class="image-slide">
										<div class="image-wrapper <?php echo $imgType; ?>">
											<?php if( $imgSrc ) {
												echo wp_get_attachment_image( $imgSrc, 'full' );
											} ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif;
}

add_action('wp_ajax_lightbox_ajax', 'lightbox_ajax');
add_action('wp_ajax_nopriv_lightbox_ajax', 'lightbox_ajax');

// add_filter('wpseo_json_ld_output', 'custom_filter_yoast_json_data', 10, 2);
// 
// 
// function custom_filter_yoast_json_data( $data, $context = false) {
// 	if($context == 'website'){ 
// 		$data['test'] = 'party';
// 	} elseif($context == 'organization') {
// 		$data['test'] = 'org';
// 	}
// 	return $data;
// }