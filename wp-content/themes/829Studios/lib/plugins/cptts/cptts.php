<?php
/*
Plugin Name: Custom Post Types, Taxonomies and Shortcodes
Plugin URI: -
Description: Register all necessary Custom Post Types, Taxonomies and Shortcodes
Version: 0.5
Author: Theme Owner
Author URI: 
License: GPL
Copyright: Theme Owner
*/

if( !class_exists('Cptts') ):

class Cptts {
	private $json;
	function __construct() {

		$this->json = $this->get_settings_json();

		add_action( 'admin_init', array( $this, 'load_textdomain' ) );
		
		add_action('init', array( $this, 'create_post_types'));
		add_action('init', array( $this, 'create_shorcodes'));
		
	}

	function load_textdomain() {
		load_plugin_textdomain( 'cptts', false, 'cptts/languages' );
	}
	
	public function create_post_types()	{
	
		 foreach( $this->json->create_post_type as $name => $post_type ) {

         $labels = array(
            'name' => _x($post_type->label_single, 'post type general name', 'cptts-admin'),
            'singular_name' => _x($post_type->label_single, 'post type singular name', 'cptts-admin'),
            'add_new' => _x('Add New', $post_type->label_single, 'cptts-admin'),
            'add_new_item' => __('Add New '.$post_type->label_single, 'cptts-admin'),
            'edit_item' => __('Edit '.$post_type->label_single, 'cptts-admin'),
            'new_item' => __('New '.$post_type->label_single, 'cptts-admin'),
            'all_items' => __('All '.$post_type->label_multi, 'cptts-admin'),
            'view_item' => __('View '.$post_type->label_single, 'cptts-admin'),
            'search_items' => __('Search '.$post_type->label_multi, 'cptts-admin'),
            'not_found' => __('No '.$post_type->label_multi.' found', 'cptts-admin'),
            'not_found_in_trash' => __('No '.$post_type->label_multi.' found in Trash', 'cptts-admin'),
            'parent_item_colon' => '',
            'menu_name' => __($post_type->menu_name, 'cptts-admin')
            );

            $arg_slug = strtolower($post_type->label_single);

            if( isset($post_type->hierarchical) ) {
                $arg_hierarchical = $post_type->hierarchical;
            } else {
                $arg_hierarchical = false;
            }
			
			if( isset($post_type->has_archive) ) {
                $arg_has_archive = $post_type->has_archive;
            } else {
                $arg_has_archive = false;
            }

            if( isset($post_type->supports) ) {
                $arg_supports = $post_type->supports;
            } else {
                $arg_supports = array('title', 'editor', 'thumbnail','page-attributes');
            }

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => _x($post_type->slug, 'URL slug', 'cptts-admin')),
                'capability_type' => 'post',
                'has_archive' => $arg_has_archive,
                'hierarchical' => $arg_hierarchical,
                'menu_position' => null,
                'supports' => $arg_supports
                );

            register_post_type($name, $args);

            if( $post_type->tax != false ) {
				
					$this->create_taxonomies($name, $post_type);
					
            }

        }
	}
	
	public function create_taxonomies($post_name, $post) {
	
		foreach($post->tax[0] as $tax_name => $taxonomy) {
		
			$tax_labels = array(
				'name' => _x( $taxonomy->tax_multi, 'taxonomy general name' ),
				'singular_name' => _x( $taxonomy->tax_single, 'taxonomy singular name' ),
				'search_items' =>  __( 'Search '.$taxonomy->tax_multi ),
				'all_items' => __( 'All '.$taxonomy->tax_multi ),
				'parent_item' => __( 'Parent '.$taxonomy->tax_single ),
				'parent_item_colon' => __( 'Parent '.$taxonomy->tax_single.':' ),
				'edit_item' => __( 'Edit '.$taxonomy->tax_single ),
				'update_item' => __( 'Update '.$taxonomy->tax_single ),
				'add_new_item' => __( 'Add New '.$taxonomy->tax_single ),
				'new_item_name' => __( 'New '.$taxonomy->tax_single.' Name' ),
				'menu_name' => __( $taxonomy->tax_single )
				);

				if( isset($taxonomy->hierarchical) ) {
					$tax_hierarchical = $taxonomy->hierarchical;
				}
				else {
					$tax_hierarchical = true;
				}

				register_taxonomy($tax_name, array($post_name), array(
					'hierarchical' => $tax_hierarchical,
					'labels' => $tax_labels,
					'show_ui' => true,
					'query_var' => true,
					'rewrite' => array( 'slug' => $taxonomy->slug ),
				 ));
		}
	}
	
	public function create_shorcodes() {
		add_shortcode('c_button', array($this, 'custom_button'));
		add_shortcode('clear', array($this, 'clear_func'));
		add_shortcode('tabs', array($this, 'tabs_func'));
		add_shortcode('tab', array($this, 'tab_func'));
	}
	
	function custom_button($atts) {
			extract(shortcode_atts(array(
			  'href' => '#',
			  'label' => __('Anchor', 'cptts'),
			  ), $atts));

			return "<a href='$href' class='custom-btn'>$label</a>";
		}
		
	function clear_func($atts, $content = "") {
	
		return "<div class='clearfix'></div>";
		
	}

	function tabs_func($atts, $content = null) {

		extract(shortcode_atts(array('titles' => ''), $atts));
		
		$titles = explode(",", $titles);
		$html = '<div class="tabs">';

		$html .= '<ul>';
		$i=1;
		foreach($titles as $title):
			$html .= '<li><a href="#tabs-'.$i.'" rel="tabs-'.$i.'">'.trim($title).'</a></li>';
		$i++;
		endforeach;
		
		$html .= '</ul>';
		$html .= do_shortcode($content);
		$html .= '</div>';
		
		return $html;
	}

	function tab_func($atts, $content = null) {

		extract(shortcode_atts(array(
		  'id' => ''), $atts));
		
		$html = '<div id="tabs-'.$id.'">';

		$html .= do_shortcode($content);
		
		$html .= '</div>';
		
		return $html;
	}
	
	function get_settings_json() {
	
		$url = dirname(__FILE__) . '/settings.json';
		$json = file_get_contents($url); 
		$json = preg_replace("/\/\*(?s).*?\*\//", "", $json);
		$json = json_decode($json);  
		
		return $json;
		
	}

}

new Cptts();

endif; // class_exists check