<?php
/**
 * The static page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<!-- =========== SINGLE PAGE ========= -->

<?php $topMenuId = get_field('select_page_menu');
      $topMenu = wp_get_nav_menu_object($topMenuId);
      $topMenuName = $topMenu->name; ?>

 <?php if ($topMenuId): ?>
 	<nav class="top-menu">
 		<?php wp_nav_menu(array("menu" => $topMenuId, 'container' => false, 'items_wrap' => '<ul class="%2$s">%3$s</ul>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
 	</nav><!-- /.top-menu -->
 	<a href="#" class="top-menu-trigger"><span><?php echo $topMenuName; ?></span></a>
 	<nav class="mobile-top-menu">
 		<?php wp_nav_menu(array("menu" => $topMenuId, 'container' => false, 'items_wrap' => '<ul class="%2$s">%3$s</ul>')); ?>
 	</nav><!-- /.mobile-top-menu -->
 <?php endif ?>

 <main class="single-page-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
		<?php if (have_rows('add_page_section')) : ?>
					<?php while(have_rows('add_page_section')) : the_row(); ?>

					<?php if (get_row_layout() == 'deafult_content') : ?>

						<!-- Default Content -->

							<?php $content = get_sub_field('section_default_content');
							$removeTopMargin = get_sub_field('remove_top_margin');
							$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
							<div class="container<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
								<div class="row">
									<div class="col-lg-12">
										<?php echo $content; ?>
									</div><!-- /.col-lg-12 -->
								</div><!-- /.row -->
							</div><!-- /.container -->

						<?php elseif (get_row_layout() == 'custom_background_color_section') : ?>

							<!-- Custom Background Color Section -->

							<?php $bgColor = get_sub_field('section_custom_bg_color_color');
							$content = get_sub_field('section_custom_bg_color_content');
							$removeTopMargin = get_sub_field('remove_top_margin');
							$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
							<section class="custom-bg-color<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>" style="background-color: <?php echo $bgColor; ?>;">
								<div class="container">
									<div class="row">
										<div class="col-lg-12">
											<?php echo $content; ?>
										</div><!-- /.col-lg-12 -->
									</div><!-- /.row -->
								</div><!-- /.container -->
							</section><!-- /.custom-bg-color -->

							<?php elseif (get_row_layout() == 'custom_background_pattern_section') : ?>

								<!-- Custom Background Pattern Section -->

								<?php $bgPatternId = get_sub_field('section_custom_bg_pattern_pattern');
								$bgPatternSrc = wp_get_attachment_image_src($bgPatternId, 'full');
								$content = get_sub_field('section_custom_bg_pattern_content');
								$removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
								<section class="custom-bg-pattern<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>" style="background: url('<?php echo $bgPatternSrc[0]; ?>');">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
												<?php echo $content; ?>
											</div><!-- .col-lg-12 -->
										</div><!-- /.row -->
									</div><!-- /.container -->
								</section><!-- .custom-bg-pattern -->

							<?php elseif (get_row_layout() == 'page_full_width_image') : ?>

								<!-- Full Width Image Section -->

								<?php $img = get_sub_field('add_page_full_width_image');
								$removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>

								<img class="page-full-width-img<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">

								<?php elseif (get_row_layout() == 'page_full_width_gallery') : ?>

								<!-- Full Width Gallery Section -->

								<?php $slideshow = get_sub_field('full_width_gallery_shortcode');
								$removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>
								<section class="full-width-gallery<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
									<?php echo do_shortcode($slideshow); ?>
								</section><!-- .full-width-gallery -->

							<?php elseif (get_row_layout() == '50_50_images') : ?>

								<!-- 50-50 Images -->

								<?php $removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>

								<section class="half-images clearfix<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
									<?php $leftImg = get_sub_field('50_50_images_left_image');
									$rightImg = get_sub_field('50_50_images_right_image'); ?>
									<img src="<?php echo $leftImg['url']; ?>" alt="<?php echo $leftImg['alt']; ?>" class="img-left">
									<img src="<?php echo $rightImg['url']; ?>" alt="<?php echo $rightImg['alt']; ?>" class="img-right">
								</section><!-- .half-images -->

								<?php elseif(get_row_layout('full_width_content')) : ?>

								<!-- Full Width Content -->

								<?php $removeTopMargin = get_sub_field('remove_top_margin');
								$removeBottomMargin = get_sub_field('remove_bottom_margin'); ?>

								<section class="full-width-content clearfix<?php if($removeTopMargin) {echo " no-top-margin";}; ?><?php if($removeBottomMargin) {echo " no-bt-margin";}; ?>">
									<?php $content = get_sub_field('section_full_width_content'); ?>

									<?php echo apply_filters('the_content', $content); ?>

								</section><!-- .full-width-content -->

								<?php endif; ?>

					<?php endwhile; ?>
				<?php endif; ?>
 </main><!-- /.page-content -->

<?php get_footer(); ?>
