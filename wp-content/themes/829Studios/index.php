<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */
get_header();
?>
<?php $sHeadText = null; ?>
<?php $blogTitle = get_field('blog_title', 'option'); ?>
<?php if($blogTitle) {
	$sHeadText = $blogTitle;
}

if(is_archive()) {
 if(is_day()) :
  $sHeadText = __('Daily Archives:', '829Studios') . get_the_date();
 elseif(is_month()) :
  $sHeadText = __('Monthly Archives:', '829Studios') . get_the_date(_x('F Y', 'monthly archives date format', '829Studios'));
 elseif(is_year()) :
  $sHeadText = __('Yearly Archives:', '829Studios') . get_the_date(_x('Y', 'yearly archives date format', '829Studios'));
 else :
  $sHeadText = __('Blog Archives', '829Studios');
 endif;
} ?>
<?php if(is_category()) {
 $sHeadText = __("Category:", "829Studios").' '.single_cat_title( '', false );
} ?>
<?php if(is_author()) {
 $authordata = get_user_by('slug', get_query_var('author_name'));
 $sHeadText = __("Author:", "829Studios").' '.$authordata->display_name;
} ?>
<?php if(is_tag()) {
 $sHeadText = __("Tag:", "829Studios").' '.single_tag_title( '', false );
} ?>
<?php if(is_search()) {
 $sHeadText = __("Search:", "829Studios").' '.get_search_query();
} ?>

<!-- =========== BLOG BANNER ========= -->

<?php $blogID = get_option('page_for_posts');
$bgVerticalPos = get_field('set_vertical_pos', $blogID);
$bgHorizontalPos = get_field('set_horizontal_pos', $blogID); ?>

<?php $blogBgId = get_field('blog_background', 'option');
$blogBgSrc = wp_get_attachment_image_src($blogBgId, 'blog-bg'); ?>

<section class="blog-section">
	<div class="blog-banner"<?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($blogBgId) : ?>background-image:url('<?php echo $blogBgSrc[0]; ?>');<?php endif; ?>"<?php endif; ?>>
		<div class="blog-banner-content">
			<?php if ($sHeadText) { ?>
			   <h1 class="blog-title"><?php echo $sHeadText; ?></h1>
			  	<?php }
			  	else { ?>
			   <h1 class="blog-title"><?php _e('Sorry, nothing found.','829Studios'); ?></h1>
			  <?php	} ?>
			  <?php get_template_part('searchform'); ?>
			</div><!-- /.blog-banner-content -->
	</div><!-- /.blog-banner -->
</section><!-- /.blog-section -->

<!-- =========== BLOG CONTENT ========= -->

<?php if (is_home() || is_category()) : ?>
	<?php $cats = get_categories(); ?>
	<nav class="blog-nav">
		<a href="#" class="cat-trigger"><?php _e('Categories', '829Studios'); ?></a>
	 <?php wp_nav_menu(array("theme_location" => 'blog-cats', 'container' => false, 'items_wrap' => '<ul class="%2$s">%3$s</ul>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
	</nav><!-- .blog-nav -->
<?php endif; ?>

<?php get_template_part('loop', 'index'); ?>

<?php if ($wp_query->max_num_pages > 1) : ?>
	<div class="load-more-wrapper">
		<a href="#" class="more-posts bt"><?php _e('load more', '829Studios'); ?></a>
	</div><!-- /.load-more-wrapper -->
<?php endif; ?>

<?php get_footer(); ?>
