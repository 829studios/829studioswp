<?php
/**
 * Template Name: About Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $bgVerticalPos = get_field('set_vertical_pos');
$bgHorizontalPos = get_field('set_horizontal_pos'); ?>

<!-- =========== ABOUT BANNER ========= -->

<?php if (have_rows('about_top_menu_sections')) : ?>
	<nav class="top-menu inner-nav">
		<ul>
			<?php $c = 1; ?>
			<?php while(have_rows('about_top_menu_sections')) : the_row(); ?>
				<?php $sectionTitle = get_sub_field('about_nav_section_title');
				$sectionLabel = get_sub_field('about_nav_section_label'); ?>
				<li><a href="#<?php echo $sectionLabel; ?>"<?php if($c == 1) : ?> class="active"<?php endif; ?>><span><?php echo $sectionTitle; ?></span></a></li>
				<?php $c++; ?>
			<?php endwhile; ?>
		</ul>
		<a href="#" class="section-nav-prev top-menu-item"></a>
		<a href="#" class="section-nav-next top-menu-item"></a>
	</nav><!-- /.top-menu -->
<?php endif; ?>

<?php $thumbId = get_post_thumbnail_id();
$thumbSrc = wp_get_attachment_image_src($thumbId, 'full'); ?>

	<section id="welcome" class="page-banner about-banner"<?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($thumbId) : ?>background-image:url('<?php echo $thumbSrc[0]; ?>');<?php endif; ?>"<?php endif; ?>>
		<div class="page-banner-content">
			<h1 class="page-title animate-title"><?php the_title(); ?></h1>
		</div><!-- /.page-banner-content -->
	</section><!-- /.page-banner -->

<!-- =========== ABOUT CONTENT ========= -->

	<div class="page-content about">
		<div class="container">
			<?php the_content(); ?>
		</div><!-- /.container-fluid -->
	</div><!-- /.page-content -->

	<?php if (have_rows('about_add_content')) : ?>
		<?php while(have_rows('about_add_content')) : the_row(); ?>

			<?php if (get_row_layout() == 'about_values') : ?>

			<?php $secTitle = get_sub_field('val_section_title');
			$secLabel = get_sub_field('val_section_label');
			$secText = get_sub_field('val_text'); ?>

			<section id="<?php echo $secLabel; ?>" class="value-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<?php if ($secTitle) : ?>
								<h2 class="section-title"><?php echo $secTitle; ?></h2>
							<?php endif; ?>
							<?php if ($secText) : ?>
								<h4><?php echo $secText; ?></h4>
							<?php endif; ?>
						</div><!-- /.col-lg-12 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
				<?php if (have_rows('val_add_values')) : ?>
					<div class="value-wrapper clearfix">
							<?php while(have_rows('val_add_values')) : the_row(); ?>
								<div class="single-value col-lg-3 col-md-3 col-sm-6 col-xs-6">
									<?php $valImg = get_sub_field('single_val_image');
									$valTitle = get_sub_field('single_val_title');
									$valText = get_sub_field('single_val_text'); ?>
									<div class="img-wrapper">
										<?php if ($valImg) : ?>
											<img src="<?php echo $valImg['url']; ?>" alt="<?php echo $$valImg['alt'];; ?>">
										<?php endif; ?>
									</div><!-- /.img-wrapper -->
									<?php if ($valTitle) : ?>
										<h6><?php echo $valTitle; ?></h6>
									<?php endif; ?>
									<?php if ($valText) : ?>
										<p><?php echo $valText; ?></p>
									<?php endif; ?>
								</div><!-- /.single-wrapper -->
							<?php endwhile; ?>

					</div><!-- /.value-wrapper -->
				<?php endif; ?>
			</section><!-- /.value-content -->

		<?php elseif (get_row_layout() == 'about_lead') : ?>

		<?php $secTitle = get_sub_field('lead_section_title');
			$secLabel = get_sub_field('lead_section_label');
			$secText = get_sub_field('lead_text'); ?>

			<section id="<?php echo $secLabel; ?>" class="lead-content">
				<?php if (have_rows('lead_add_person')) : ?>

					<?php $c = 1; ?>
					<?php while(have_rows('lead_add_person')) : the_row(); ?>
						<?php $lightboxText = get_sub_field('lead_ligthbox_text');
						$lightboxPhoto = get_sub_field('lead_lightbox_photo');
						$personTitle = get_sub_field('lead_person_title');
						$personName = get_sub_field('lead_person_name'); ?>
						<div class="container">
								<?php if ($lightboxText): ?>
									<div class="person-info col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1" data-tab="person-<?php echo $c; ?>">
										<a href="#" class="close-bt"></a>
											<?php if ($lightboxPhoto) : ?>
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 photo-box">
													<img src="<?php echo $lightboxPhoto['url']; ?>" alt="<?php echo $lightboxPhoto['alt'] ?>">
												</div><!-- /.col-lg-4 -->
											<?php endif; ?>
											<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 person-text">
												<?php if ($personName) : ?>
													<h4><?php echo $personName; ?></h4>
												<?php endif; ?>
												<?php if ($personTitle) : ?>
													<h6><?php echo $personTitle; ?></h6>
												<?php endif; ?>
												<?php echo $lightboxText; ?>
											</div><!-- /.col-lg-7 -->
									</div><!-- /.col-lg-10 -->
								<?php endif; ?>
						</div><!-- /.container -->
						<?php $c++; ?>
					<?php endwhile; ?>

			<?php endif; ?>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<?php if ($secTitle) : ?>
								<h2 class="section-title"><?php echo $secTitle; ?></h2>
							<?php endif; ?>
							<?php if ($secText) : ?>
								<h4><?php echo $secText; ?></h4>
							<?php endif; ?>
						</div><!-- /.col-lg-12 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
				<?php if (have_rows('lead_add_person')) : ?>
					<div class="person-wrapper clearfix">
						<?php $c = 1; ?>
						<?php while(have_rows('lead_add_person')) : the_row(); ?>
						<?php $photoImgId = get_sub_field('lead_person_photo');
						$photoImg = wp_get_attachment_image($photoImgId, 'full');
						$personTitle = get_sub_field('lead_person_title');
						$personName = get_sub_field('lead_person_name');
						$lightboxText = get_sub_field('lead_ligthbox_text'); ?>
							<div class="person-box">
							<?php echo $photoImg; ?>
								<div class="text-box">
									<?php if ($personTitle) : ?>
										<h6><?php echo $personTitle; ?></h6>
									<?php endif; ?>
									<?php if ($personName) : ?>
										<h4><?php echo $personName; ?></h4>
									<?php endif; ?>
									<?php if ($lightboxText) : ?>
										<a href="#" class="bt" data-tab="person-<?php echo $c; ?>"><?php _e('Read Bio', '829Studios'); ?></a>
									<?php endif; ?>
								</div>
							</div><!-- /.person-box -->
							<?php $c++; ?>
						<?php endwhile; ?>
					</div><!-- /.person-wrapper -->
				<?php endif; ?>
			</section><!-- /.lead-content -->

			<?php elseif (get_row_layout() == 'about_text_slider') : ?>

			<?php $secLabel = get_sub_field('slider_sec_section_label');
			$text = get_sub_field('about_slider_text');
			$sliderSide = get_sub_field('about_text_slideshow_side');
			$btLabel = get_sub_field('about_bt_label');
			$btLink = get_sub_field('about_bt_link'); ?>

			<section<?php if($secLabel) : ?> id="<?php echo $secLabel; ?>"<?php endif; ?> class="workspace-content clearfix">
				<?php if ($sliderSide == 'right') : ?>
					<div class="workspace-text">
						<div class="text-wrapper">
							<?php echo $text; ?>
						</div><!-- /.text-wrapper -->
					</div><!-- /.workspace-text -->
					<?php if (have_rows('about_work_slideshow')) : ?>
						<div class="slider-wrapper">
							<div class="page-gallery">
								<?php while(have_rows('about_work_slideshow')) : the_row(); ?>
								<?php $slide = get_sub_field('about_work_slide'); ?>
									<div style="background-image: url('<?php echo $slide; ?>');"></div>
								<?php endwhile; ?>
							</div>
						</div><!-- /.slider-wrapper -->
					<?php endif; ?>
					<?php else : ?>
					<?php if (have_rows('about_work_slideshow')) : ?>
						<div class="slider-wrapper left-slider">
							<div class="page-gallery">
								<?php while(have_rows('about_work_slideshow')) : the_row(); ?>
								<?php $slide = get_sub_field('about_work_slide'); ?>
									<div style="background-image: url('<?php echo $slide; ?>');"></div>
								<?php endwhile; ?>
							</div>
						</div><!-- /.slider-wrapper -->
					<?php endif; ?>
					<div class="workspace-text">
						<div class="text-wrapper">
							<?php echo $text; ?>
						</div><!-- /.text-wrapper -->
					</div><!-- /.workspace-text -->
				<?php endif; ?>
				<?php if ($btLabel && $btLink) : ?>
				<div class="bt-wrapper">
					<a href="<?php echo $btLink ?>" class="bt"><?php echo $btLabel; ?></a>
				</div>
				<?php endif; ?>
			</section><!-- /.workspace-content -->


			<?php endif; ?>

		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
