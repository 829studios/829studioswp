<?php

// check if the flexible content field has rows of data
if( have_rows('case_study_sections') ):

     // loop through the rows of data
    while ( have_rows('case_study_sections') ) : the_row();

		$file = dirname( __FILE__ ) . "/case_study-sections/" . get_row_layout() . ".php";

		if( file_exists( $file ) ){
			require $file;
		}else{
			echo "Tere is no template file for section '" . get_row_layout() . "'. Template should be in: " . $file;
		}

    endwhile;

endif;
