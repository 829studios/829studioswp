<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage 829Studios
 * @since 829Studios 1.0
 */

get_header(); the_post(); ?>

<?php $currentService = get_term_by('slug', get_query_var('service'), 'service');
$currentServiceId = $currentService->term_taxonomy_id; ?>

<!-- =========== SERVICE BANNER ========= -->

<?php $servicesParentArgs = array('parent' => 0, 'hide_empty' => false);
		$parentServices = get_terms('service', $servicesParentArgs); ?>

<?php if (!empty($parentServices)) : ?>
	<nav class="top-menu services-menu">
		<ul>
			<?php foreach($parentServices as $service) : ?>
			<?php $serviceId = $service->term_id;
			$serviceIcon = get_field('service_icon_alt', 'service_'.$serviceId);
			$serviceIconIE = get_field('service_icon_alt_ie', 'service_'.$serviceId);
			$serviceName = $service->name; ?>
			<li<?php if($currentServiceId == $serviceId) : ?> class="current-menu-item"<?php endif; ?>><a href="<?php echo get_term_link($service->slug, 'service'); ?>">
			<?php if ($serviceIcon) : ?>
				<span class="service-menu-icon hide-ie"><img src="<?php echo $serviceIcon['url'] ?>"></span>
			<?php endif; ?>
			<!--[if lt IE 9]>
				<?php if($serviceIconIE) : ?>
					<span class="service-menu-icon"><img src="<?php echo $serviceIconIE['url']; ?>" alt="<?php echo $serviceIconIE['alt']; ?>"></span>
				<?php endif; ?>
				<!<![endif]-->
			<span class="service-menu-name"><?php echo $serviceName; ?></span>
		</a></li>
			<?php endforeach; ?>
		</ul>
	</nav><!-- /.top-menu -->
<?php endif; ?>

<?php $bgVerticalPos = get_field('set_vertical_pos', 'service_'.$currentServiceId);
$bgHorizontalPos = get_field('set_horizontal_pos', 'service_'.$currentServiceId);
$serviceBg = get_field('service_background', 'service_'.$currentServiceId);
$serviceIcon = get_field('service_icon_alt', 'service_'.$currentServiceId);
$serviceIconIE = get_field('service_icon_alt_ie', 'service_'.$currentServiceId); ?>

<section id="single-service-banner" class="page-banner services-banner"<?php if($bgVerticalPos && $bgHorizontalPos) : ?> style="background-position: <?php echo $bgHorizontalPos; ?> <?php echo $bgVerticalPos; ?>; <?php if($serviceBg) : ?>background-image:url('<?php echo $serviceBg; ?>');<?php endif; ?>"<?php endif; ?>>
	<div class="page-banner-content">
		<?php if ($serviceIcon) : ?>
			<object data="<?php echo $serviceIcon['url'] ?>" type="image/svg+xml" class="animate-title"><img src="<?php echo $serviceIcon['url'] ?>"></object>
		<?php endif; ?>
		<!--[if lt IE 9]>
			<?php if($serviceIconIE) : ?>
				<img src="<?php echo $serviceIconIE['url']; ?>" alt="<?php echo $serviceIconIE['alt']; ?>">
			<?php endif; ?>
			<!<![endif]-->
		<h1 class="page-title animate-title"><?php echo $currentService->name; ?></h1>
	</div><!-- /.page-banner-content -->
</section><!-- /.page-banner -->

<!-- =========== SERVICE CONTENT ========= -->

<section id="single-service-content">
	<div class="container">
		<div class="row">
			<?php $intro = get_field('service_intro', 'service_'.$currentServiceId); ?>
			<?php if ($intro) : ?>
				<div class="service-intro">
					<?php echo $intro; ?>
				</div><!-- /.service-intro -->
			<?php endif; ?>

			<?php if (have_rows('service_add_work', 'service_'.$currentServiceId)) : ?>
				<div class="box-info-wrapper clearfix">
					<?php while(have_rows('service_add_work', 'service_'.$currentServiceId)) : the_row(); ?>
						<div class="single-box-info col-lg-4 col-md-4 col-sm-6 col-xs-6">
							<?php $title = get_sub_field('service_single_work_title');
							$text = get_sub_field('service_single_work_text');
							$boxImg = get_sub_field('service_single_work_image'); ?>
							<?php if ($boxImg) : ?>
								<img src="<?php echo $boxImg['url']; ?>" alt="<?php echo $boxImg['alt']; ?>">
							<?php endif ?>
								<h6><?php echo $title; ?></h6>
								<p><?php echo stripteaser($text, 150); ?></p>
						</div><!-- /.single-box-info -->
					<?php endwhile; ?>
				</div><!-- /.box-info-wrapper -->
			<?php endif; ?>




		</div><!-- /.row -->
	</div><!-- /.container -->

	<?php $sectionText = get_field('example_section_text', 'service_'.$currentServiceId); ?>

	<?php if ($sectionText) : ?>
		<section id="works-content">
			<div class="container">
				<?php echo $sectionText; ?>
			</div><!-- /.container -->
		</section><!-- /#works-content -->
	<?php endif; ?>

	<?php if (have_rows('add_example', 'service_'.$currentServiceId)) : ?>
		<section id="primary-works">
			<?php while(have_rows('add_example', 'service_'.$currentServiceId)) : the_row(); ?>
			<?php $workBgId = get_sub_field('example_background');
			$workBgSrc = wp_get_attachment_image_src($workBgId, 'full');
			$workLogo = get_sub_field('example_logo');
			$workTitle = get_sub_field('example_title');
			$workDesc = get_sub_field('example_description');
			$workUrl = get_sub_field('example_read_more_url');
			$hoverColor = get_sub_field('example_hover_color');  ?>
			<div class="single-primary-work"<?php if($workBgSrc) : ?> style="background-image: url('<?php echo $workBgSrc[0]; ?>');"<?php endif; ?>>
				<?php if ($workLogo) : ?>
					<span class="logo-wrapper">
						<img src="<?php echo $workLogo['url']; ?>" alt="<?php echo $workLogo['alt']; ?>">
					</span>
				<?php endif; ?>
				<div class="box-content">
					<?php if ($hoverColor) : ?>
						<span class="box-bg" style="background-color: <?php echo $hoverColor; ?>;"></span>
					<?php endif; ?>
					<div class="box-text-wrapper">
						<?php if ($workTitle) { echo "<h4>".$workTitle."</h4>"; } ?>
						<?php if ($workDesc) { echo $workDesc; } ?>
					</div><!-- /.box-text-wrapper -->
						<div class="box-text-link">
							<?php if ($workUrl) { echo "<a class='bt' href=".$workUrl.">".__("read more", "829Studios")."</a>"; } ?>
						</div><!-- /.box-text-link -->
				</div><!-- /.box-content -->
			</div><!-- /.single-primary-work -->
			<?php endwhile; ?>
		</section><!-- /#primary-works -->
	<?php endif; ?>

</section><!-- #single-service-content -->

<?php get_footer(); ?>
